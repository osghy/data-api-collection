package com;

import com.base.BaseTests;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.ghy.ic.data.contoller.managedata.ManageTagController;
import com.ghy.ic.data.iface.managedata.ManageTagApiService;
import com.info.center.data.sys.contract.constant.TagTypeEnum;
import com.info.center.data.sys.contract.model.ManageTagModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * create by huweiqiang on 2018/12/26
 */
@SpringBootTest(classes = ManageTagController.class)
@ContextConfiguration(locations = {"classpath:spring.xml"})
public class Test extends BaseTests {

    @Autowired
    private ManageTagController controller;

    @Autowired
    private ManageTagApiService tagApiService;

    @org.junit.Test
    public void test() throws JsonProcessingException {
        controller.queryTags();

        ManageTagModel model = new ManageTagModel();
        model.setType(TagTypeEnum.SUBJECT.value());
        model.setTagName("人口");
        tagApiService.addManageTag(model);
    }


}
