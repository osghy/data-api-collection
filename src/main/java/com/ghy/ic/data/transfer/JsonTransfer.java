package com.ghy.ic.data.transfer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ghy.ic.data.domain.form.UserModel;
import com.info.center.data.sys.contract.model.RespCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * create by huweiqiang on 2018/12/21
 */
public class JsonTransfer {
    private static final Logger logger = LoggerFactory.getLogger(JsonTransfer.class);

    public static String buildJosn(String flag, UserModel userModel) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("status", flag);
        if (flag.equals("error")) {
            objectNode.put("currentAuthority", "guest");
        } else {
            objectNode.put("currentAuthority", "user");
            objectNode.put("userId", userModel.getUserId());
            objectNode.put("userName", userModel.getUserName());
            try {
                objectNode.put("userRoles", objectMapper.writeValueAsString(userModel.getUserRoles()));
            } catch (JsonProcessingException e) {
                logger.error("=====>数组转json失败:", e);
            }
        }
        objectNode.put("type", "account");
        logger.info("======>登录信息:{}", objectNode.toString());
        return objectNode.toString();
    }

    public static String result(RespCommon respCommon) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode node = objectMapper.createObjectNode();
        if (respCommon.isInvokeSuccess()) {
            node.put("msg", respCommon.getInvokeInfo());
        } else {
            node.put("msg", respCommon.getErrMsg());
        }
        return node.toString();
    }
}
