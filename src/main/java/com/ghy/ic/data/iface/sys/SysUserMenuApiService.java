package com.ghy.ic.data.iface.sys;

import info.center.data.sys.contract.iface.SysUserMenuService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * create by huweiqiang on 2018/12/12
 */
@FeignClient(value = "${sys.manage.service}", url = "${sys.manage.service.url}")
public interface SysUserMenuApiService extends SysUserMenuService {
}
