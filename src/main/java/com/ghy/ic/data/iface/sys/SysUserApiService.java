package com.ghy.ic.data.iface.sys;

import info.center.data.sys.contract.iface.SysUserService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * create by hecore on 2018/12/3
 */
@FeignClient(value = "${sys.manage.service}", url = "${sys.manage.service.url}")
public interface SysUserApiService extends SysUserService {
}
