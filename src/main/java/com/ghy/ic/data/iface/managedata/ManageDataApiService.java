package com.ghy.ic.data.iface.managedata;

import com.info.center.data.sys.contract.iface.ManageDataService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * create by huweiqiang on 2018/12/17
 */
@FeignClient(value = "${data.manage.service}", url = "${data.manage.service.url}")
public interface ManageDataApiService extends ManageDataService {

}
