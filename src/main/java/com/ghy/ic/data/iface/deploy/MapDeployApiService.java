package com.ghy.ic.data.iface.deploy;

import com.info.center.data.deploy.contract.iface.MapDeployService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(value = "data-deploy-service")
public interface MapDeployApiService extends MapDeployService {
}
