package com.ghy.ic.data.iface.managedata;

import com.info.center.data.sys.contract.iface.ManageProjectService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author Y-Tree
 * @date 18-12-28 下午7:18
 */
@FeignClient(value = "${data.manage.service}", url = "${data.manage.service.url}")
public interface ManageProjectApiService extends ManageProjectService {
}
