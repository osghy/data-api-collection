package com.ghy.ic.data.iface.managedata;

import com.info.center.data.sys.contract.iface.ManageTagService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * create by huweiqiang on 2018/12/26
 */
@FeignClient(value = "${data.manage.service}", url = "${data.manage.service.url}")
public interface ManageTagApiService extends ManageTagService {
}
