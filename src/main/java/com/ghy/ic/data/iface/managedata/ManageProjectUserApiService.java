package com.ghy.ic.data.iface.managedata;

import com.info.center.data.sys.contract.iface.ManageProjectUserService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author Y-Tree
 * @date 19-1-2 下午4:13
 */
@FeignClient(value = "${data.manage.service}", url = "${data.manage.service.url}")
public interface ManageProjectUserApiService extends ManageProjectUserService {
}
