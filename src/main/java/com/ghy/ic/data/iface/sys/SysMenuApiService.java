package com.ghy.ic.data.iface.sys;

import info.center.data.sys.contract.iface.SysMenuService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * create by huweiqiang on 2018/11/26
 */
@FeignClient(value = "${sys.manage.service}", url = "${sys.manage.service.url}")
public interface SysMenuApiService extends SysMenuService {
}
