package com.ghy.ic.data.iface.sys;

import info.center.data.sys.contract.iface.SysRoleService;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * create by huweiqiang on 2018/12/7
 */
@FeignClient(value = "${sys.manage.service}", url = "${sys.manage.service.url}")
public interface SysRoleApiService extends SysRoleService {
}
