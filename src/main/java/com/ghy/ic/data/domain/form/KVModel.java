package com.ghy.ic.data.domain.form;

import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/12/4
 */
@Setter
@Getter
public class KVModel {

    private String key;
    private String value;
    private String title;

}
