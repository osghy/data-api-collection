package com.ghy.ic.data.domain.form;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * create by huweiqiang on 2019/1/3
 */
@Setter
@Getter
public class DataFormModel {

    private Integer userId;
    private List<Integer> dates;
    private List<Integer> subjects;
    private String orderBy;
    private Integer pageIndex;
    private Integer pageSize;

}
