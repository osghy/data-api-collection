package com.ghy.ic.data.domain.manage;

import com.ghy.ic.data.iface.sys.SysMenuApiService;
import com.ghy.ic.data.iface.sys.SysUserMenuApiService;
import info.center.data.sys.contract.model.SysUserMenuModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

/**
 * create by huweiqiang on 2018/12/19
 */
public class UserMenuManager {
    @Autowired
    private SysMenuApiService sysMenuApiService;
    @Autowired
    private SysUserMenuApiService userMenuApiService;

    /**
     * 根据菜单ID获取所有父ID集合
     */
    public List<Integer> getParentList(int menuId) {
        //根据菜单ID获取所有父ID集合
        List<Integer> parentIdList = sysMenuApiService.queryParentIdList(menuId);
        //获取中间表所有信息
        List<SysUserMenuModel> userMenuModelList = userMenuApiService.queryUserMenu(new SysUserMenuModel());
        List<Integer> menuIdList = userMenuModelList.stream().map(SysUserMenuModel::getMenuId).collect(Collectors.toList());
        //parentIdList不在menuIdList中的元素
        parentIdList.removeAll(menuIdList);
        return parentIdList;
    }
}
