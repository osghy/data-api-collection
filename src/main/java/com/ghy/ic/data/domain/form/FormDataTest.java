package com.ghy.ic.data.domain.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * create by huweiqiang on 2019/1/15
 */
@Setter
@Getter
public class FormDataTest {
    private String title;
    private MultipartFile[] files;

}
