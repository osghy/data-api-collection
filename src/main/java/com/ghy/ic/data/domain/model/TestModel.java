package com.ghy.ic.data.domain.model;

import com.ghy.ic.data.util.excel.ColumnSetting;
import com.ghy.ic.data.util.excel.ExcelExportedAble;
import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/10/24
 */
@Setter
@Getter
public class TestModel implements ExcelExportedAble {

    //  付款单Id
    @ColumnSetting(name = "请款单编码")
    private String id;
    //  供应商Id
    @ColumnSetting(name = "供应商编码")
    private String supplierId;

}
