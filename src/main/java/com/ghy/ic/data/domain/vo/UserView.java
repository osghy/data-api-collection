package com.ghy.ic.data.domain.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserView {
    private Integer id;
    private String loginName;//登录名
    private String userName;//用户名
    private String password;//密码
    private String deptMap; //部门Id
    private String syncFlag;//是否同步
    private String level;//用户级别
    private String[] menuConf;//多选用[]
    private String roleConf;//用户角色权限
}
