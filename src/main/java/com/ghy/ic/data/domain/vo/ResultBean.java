package com.ghy.ic.data.domain.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResultBean {
    String msg;
    int code;
}
