package com.ghy.ic.data.domain.form;

import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/12/21
 */
@Setter
@Getter
public class LoginModel {

    private String userName;
    private String password;
    private String type;

}
