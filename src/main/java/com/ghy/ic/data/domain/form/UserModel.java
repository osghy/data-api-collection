package com.ghy.ic.data.domain.form;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * create by huweiqiang on 2019/1/29
 */
@Setter
@Getter
public class UserModel {
    private Integer userId;
    private List<String> userRoles;
    private String userName;
}
