package com.ghy.ic.data.domain.form;

import lombok.Getter;
import lombok.Setter;

/**
 * create by huweiqiang on 2018/5/22
 */
@Setter
@Getter
public class SearchForm {

    private int pageSize;
    private int total;
    private int pageIndex;

    public int  getPageIndex(){
        return  this.pageIndex = total/pageSize * pageSize;
    }
}
