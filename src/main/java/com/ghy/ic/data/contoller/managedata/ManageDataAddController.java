package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.vo.ResponseResult;
import com.ghy.ic.data.iface.deploy.MapDeployApiService;
import com.ghy.ic.data.iface.managedata.ManageDataApiService;
import com.ghy.ic.data.iface.managedata.ManageTagApiService;
import com.ghy.ic.data.iface.sys.SysUserApiService;
import com.info.center.data.deploy.contract.model.DeployShpModel;
import com.info.center.data.deploy.contract.model.SaveShpModel;
import com.info.center.data.sys.contract.constant.DataTypeEnum;
import com.info.center.data.sys.contract.constant.TagTypeEnum;
import com.info.center.data.sys.contract.model.ManageDataModel;
import com.info.center.data.sys.contract.model.ManageDataTagModel;
import com.info.center.data.sys.contract.model.ManageTagModel;
import info.center.data.sys.contract.model.SysUserModel;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Y-Tree
 * @date 18-12-28 上午11:06
 */
@RestController
@RequestMapping("/manage/data")
public class ManageDataAddController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageDataAddController.class);

    @Autowired
    private ManageTagApiService manageTagApiService;
    @Autowired
    private MapDeployApiService mapDeployApiService;
    @Autowired
    private ManageDataApiService manageDataApiService;
    @Autowired
    private SysUserApiService sysUserApiService;
    @Value("${uploadBasePath}")
    private String uploadBasePath;

    /**
     * 添加标签
     */
    @RequestMapping(value = "tag/add")
    public String addTag(@RequestBody String param) throws IOException {

        LOGGER.info("===============>添加标签{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        ManageTagModel model = objectMapper.readValue(param, ManageTagModel.class);
        model.setType(TagTypeEnum.SUBJECT.value());
        // int insertCount = manageTagApiService.addManageTag(model);
        return String.valueOf("");
    }

    /**
     * 添加地图发布信息
     */
    @RequestMapping(value = "add")
    public String add(HttpServletRequest request) throws IOException {
        MultipartHttpServletRequest params = ((MultipartHttpServletRequest)request);
        List<MultipartFile> files =  ((MultipartHttpServletRequest)request).getFiles("files");
        LOGGER.info("===============>添加发布信息{}", params);
        String title = params.getParameter("title");
        int userId = Integer.parseInt(params.getParameter("userId"));
        int dataType = Integer.parseInt(params.getParameter("dataType"));
        String dataSource = params.getParameter("dataSource");
        String coordinate = params.getParameter("coordinate");
        String prdDate = params.getParameter("prdDate");
        String speciallySubject = params.getParameter("speciallySubject");

        ManageDataModel model = new ManageDataModel();
        model.setUserId(userId);
        model.setTitle(title);
        model.setDataSource(dataSource);
        model.setCoordinate(coordinate);
        model.setPrdDate(prdDate);
        model.setSpeciallySubject(speciallySubject);
        model.setDataType(DataTypeEnum.valueOf(dataType).value());
        model.setVersion(0);
        model.setCount(0);
        List<ManageDataTagModel> dataTags = new ArrayList<>();
        String[] subjects = speciallySubject.split(",");
        for (String subject : subjects){
            ManageDataTagModel tagModel = new ManageDataTagModel();
            tagModel.setTagId(Integer.parseInt(subject));
            dataTags.add(tagModel);
        }
        model.setDataTags(dataTags);
        if (dataType == DataTypeEnum.PROJECT.value()){
            int projectId = Integer.parseInt(params.getParameter("projectId"));
            model.setProjectId(projectId);
        }

        SysUserModel userModel = sysUserApiService.findUserById(model.getUserId());
        String userName = userModel.getUserName();
        String loginName = userModel.getLoginName();
        Integer deptId = userModel.getDeptId();
        String deptName = userModel.getDeptName();
        model.setUserName(userName);
        model.setDeptId(deptId);
        model.setDeptName(deptName);
        int id = manageDataApiService.addManageData(model);
        model.setId(id);

        ObjectMapper objectMapper = new ObjectMapper();
        if (id <= 0){
            LOGGER.info("===============>添加发布信息失败{}", params);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        String mapFilePath = upload(files, loginName, id);
        if ("".equals(mapFilePath)){
            LOGGER.info("===============>添加发布信息, 上传文件失败{}", params);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        String[] temp = mapFilePath.split("\\\\");
        String mapFileName = temp[temp.length - 1];
        LOGGER.info("===============>map file name : {}", mapFileName);
        model.setMapFileName(mapFileName);
        model.setMapFilePath(mapFilePath);
        if (!manageDataApiService.updateManageDataById(model)){
            LOGGER.info("===============>添加发布信息, 更新失败{}", params);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        LOGGER.info("===============>添加发布信息成功{}", params);
        return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
    }

    /**
     * 下载zip
     */
    @RequestMapping(value = "download/{id}")
    public void download(@PathVariable("id") int id, HttpServletResponse response){

        LOGGER.info("===============>下载文件{}", id);
        ManageDataModel model = manageDataApiService.queryManageDataById(id);
        FileManage fileManage = new FileManage();
        if(!fileManage.download(response, model.getMapFilePath(), model.getMapFileName())){
            LOGGER.info("===============>下载文件失败{}", id);
        }
        LOGGER.info("===============>下载文件成功{}", id);
    }

    /**
     * 上传多个文件
     * @param files 上传的多个文件
     * @param loginName 用户名
     * @param projectId 项目id
     * @return boolean
     */
    private String upload(List<MultipartFile> files, String loginName, int projectId){

        DateTime dt = new DateTime();

        // 0. 获取文件名称
        String fileName = files.get(0).getOriginalFilename().split("\\.")[0];
        // 1. 上传文件的保存路径设置
        int uploadDate = dt.getYear();
        long uploadTimeStamp = dt.getMillis();
        String saveDirPath = new StringBuilder()
                .append(uploadBasePath).append("\\")
                .append(uploadDate).append("\\")
                .append(uploadTimeStamp).append("\\")
                .append(fileName).append("\\").toString();

        LOGGER.info("===============>上传文件保存路径：{}", saveDirPath);

        // 2. 保存文件
        File saveDir = new File(saveDirPath);
        if (!saveDir.exists()){
            saveDir.mkdirs();
            LOGGER.info("===============>上传文件夹创建成功：{}", saveDirPath);
        }

        for (MultipartFile file : files) {

            if (file.isEmpty()){
                LOGGER.error("===============>文件为空");
                return "";
            }
            String filename = file.getOriginalFilename();
            File destFile = new File(saveDirPath + filename);
            try {
                file.transferTo(destFile);
                LOGGER.info("===============>{} 保存成功", filename);
            } catch (IOException e) {
                LOGGER.error("===============>{} 保存失败", filename);
                LOGGER.error("===============>保存文件异常信息：{}", e);
                return "";
            }
        }

        // 3. 调用shp发布接口
        String serverName = new DateTime().getMillis() + "";
        SaveShpModel saveShpModel = new SaveShpModel();
        saveShpModel.setEncode("UTF-8");
        saveShpModel.setDbName("geodb");
        saveShpModel.setAbsPathShape(saveDirPath);
        saveShpModel.setServerName(serverName);
        saveShpModel.setShapeName(fileName);
        mapDeployApiService.saveShpToDb(saveShpModel);

        DeployShpModel deployShpModel = new DeployShpModel();
        deployShpModel.setIp("localhost");
        deployShpModel.setMapPng(saveDirPath+"png\\"+fileName+".png");
        deployShpModel.setMxdPath(saveDirPath+"mxd\\"+fileName+".mxd");
        deployShpModel.setServerName(serverName);
        deployShpModel.setShpeFile(saveDirPath+fileName+".shp");
        deployShpModel.setId(projectId);
        mapDeployApiService.deployShp(deployShpModel);
        return saveDirPath;
    }
}
