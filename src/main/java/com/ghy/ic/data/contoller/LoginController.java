package com.ghy.ic.data.contoller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.form.LoginModel;
import com.ghy.ic.data.domain.form.UserModel;
import com.ghy.ic.data.iface.sys.SysRoleApiService;
import com.ghy.ic.data.iface.sys.SysUserApiService;
import com.ghy.ic.data.iface.sys.SysUserRoleApiService;
import com.ghy.ic.data.transfer.JsonTransfer;
import info.center.data.sys.contract.model.SysRoleModel;
import info.center.data.sys.contract.model.SysUserModel;
import info.center.data.sys.contract.model.SysUserRoleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * create by huweiqiang on 2018/12/21
 */
@RestController
@RequestMapping("/user")
public class LoginController {
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    private SysUserApiService userApiService;
    @Autowired
    private SysUserRoleApiService userRoleApiService;
    @Autowired
    private SysRoleApiService roleApiService;

    @RequestMapping("/login")
    public String login(@RequestBody String param, HttpServletRequest request) throws IOException {
        logger.info("==========>用户:{}开始登录", param);
        ObjectMapper objectMapper = new ObjectMapper();
        LoginModel loginModel = objectMapper.readValue(param, LoginModel.class);
        SysUserModel model = new SysUserModel();
        model.setLoginName(loginModel.getUserName());
        List<SysUserModel> list = userApiService.queryUsers(model);
        if (!list.isEmpty()) {
            Optional<SysUserModel> userModelOptional = list.stream().findFirst();
            String password = userModelOptional.get().getPassword();
            if (loginModel.getPassword().equals(password)) {
                logger.info("==========>登录成功", param);
                HttpSession session = request.getSession();
                session.setAttribute("user", model.getLoginName());
                return JsonTransfer.buildJosn("ok", getUserModel(userModelOptional.get()));
            }
        }
        logger.info("==========>登录失败", param);
        return JsonTransfer.buildJosn("error", null);
    }

    private UserModel getUserModel(SysUserModel sysUserModel) {
        UserModel userModel = new UserModel();
        userModel.setUserId(sysUserModel.getId());
        BeanUtils.copyProperties(sysUserModel, userModel);
        //查看用户角色
        SysUserRoleModel userRoleModel = new SysUserRoleModel();
        userRoleModel.setUserId(sysUserModel.getId());
        List<SysUserRoleModel> userRoleModelList = userRoleApiService.querySysUserRoleList(userRoleModel);
        if (!userRoleModelList.isEmpty()) {
            List<Integer> list = userRoleModelList.stream().map(SysUserRoleModel::getRoleId).collect(Collectors.toList());
            SysRoleModel roleModel = new SysRoleModel();
            roleModel.setIds(list);
            List<SysRoleModel> roleModelList = roleApiService.queryRoleList(roleModel);
            List<String> roleNameList = roleModelList.stream().map(SysRoleModel::getRoleName).collect(Collectors.toList());
            userModel.setUserRoles(roleNameList);//用户角色集合
        }
        return userModel;
    }

}
