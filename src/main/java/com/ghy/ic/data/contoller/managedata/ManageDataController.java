package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.iface.managedata.ManageDataApiService;
import com.info.center.data.sys.contract.constant.DataTypeEnum;
import com.info.center.data.sys.contract.model.ManageDataModel;
import info.center.data.sys.contract.iface.SysUserService;
import info.center.data.sys.contract.model.SysUserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * create by huweiqiang on 2018/12/17
 */
@RestController
@RequestMapping("/manage/data")
public class ManageDataController {
    private static final Logger logger = LoggerFactory.getLogger(ManageDataController.class);
    @Autowired
    private ManageDataApiService manageDataApiService;
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("/list")
    public String list(@RequestBody String param) throws IOException {
        logger.info("查询参数:{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        ManageDataModel dataModel = objectMapper.readValue(param, ManageDataModel.class);
        dataModel.setDataType(DataTypeEnum.valueOf(dataModel.getDataType()).value());
        // 业务所数据
        if (dataModel.getDataType() == DataTypeEnum.DEPT.value()) {
            int userId = dataModel.getUserId();
            SysUserModel sysUserModel = sysUserService.findUserById(userId);
            dataModel.setDeptId(sysUserModel.getDeptId());
        }
        List<ManageDataModel> list = manageDataApiService.queryManageData(dataModel);
        String response = objectMapper.writeValueAsString(list);
        return response;
    }

    /**
     * 不区分用户，点一次加一次
     */
    @RequestMapping("/updateCountById/{id}")
    public String updateCountById(@PathVariable("id") int id) {
        int count = manageDataApiService.updateCountById(id);
        return "";
    }

    @RequestMapping("/delete/{dataType}/{id}")
    public String delete(@PathVariable("dataType") int dataType, @PathVariable("id") int id) {
        boolean boo = manageDataApiService.deleteManageDataById(dataType, id);
        return "";
    }


}
