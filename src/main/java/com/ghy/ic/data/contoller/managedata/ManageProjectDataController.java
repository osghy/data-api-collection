package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.vo.ResponseResult;
import com.ghy.ic.data.iface.managedata.ManageProjectDataApiService;
import com.info.center.data.sys.contract.model.ManageProjectDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * @author Y-Tree
 * @date 19-1-2 下午4:20
 */
@RestController
@RequestMapping("/manage/project/data")
public class ManageProjectDataController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageProjectDataController.class);

    @Autowired
    private ManageProjectDataApiService manageProjectDataApiService;

    /**
     * 创建项目和数据关联信息记录
     * @param param 项目和数据关联信息串
     * @return String
     * @throws IOException json解析异常
     */
    @PostMapping(value = "add")
    public String addManageProjectData(@RequestBody String param) throws IOException {

        LOGGER.info("===============>添加数据{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        ManageProjectDataModel model = objectMapper.readValue(param, ManageProjectDataModel.class);
        int id = manageProjectDataApiService.addManageProjectData(model);
        if (id <= 0){
            LOGGER.error("===============>添加失败.");
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }else {
            LOGGER.info("===============>添加成功.");
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
    }

    /**
     * 根据项目id获取项目和数据关联记录列表
     * @param projectId 项目id
     * @return String
     * @throws JsonProcessingException 对象转换json串异常
     */
    @GetMapping(value = "getList/{projectId}")
    public String queryManageProjectDatasByProjectId(@PathVariable("projectId") int projectId) throws JsonProcessingException {

        LOGGER.info("===============>根据projectId={}获取数据集", projectId);
        List<ManageProjectDataModel> models = manageProjectDataApiService.queryManageProjectDatasByProjectId(projectId);
        ObjectMapper objectMapper = new ObjectMapper();
        if (models.isEmpty()){
            LOGGER.error("===============>根据projectId={}获取数据集失败.", projectId);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        LOGGER.info("===============>根据projectId={}获取数据集成功.", projectId);
        return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok", models));
    }

    /**
     * 根据项目和数据关联记录id获取数据
     * @param id 项目和数据关联记录id
     * @return String
     * @throws JsonProcessingException 对象转换json串异常
     */
    @GetMapping(value = "get/{id}")
    public String queryManageProjectDataById(@PathVariable("id") int id) throws JsonProcessingException {

        LOGGER.info("===============>根据id={}获取数据", id);
        ManageProjectDataModel model = manageProjectDataApiService.queryManageProjectDataById(id);
        ObjectMapper objectMapper = new ObjectMapper();
        if (model == null){
            LOGGER.error("===============>根据id={}获取数据失败.", id);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        LOGGER.info("===============>根据id={}获取数据成功", id);
        return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok", model));
    }

    /**
     * 更具项目id删除数据集
     * @param projectId 项目id
     * @return String
     */
    @GetMapping(value = "deleteAll/{projectId}")
    public String deleteManageProjectDataByProjectId(@PathVariable("projectId") int projectId) throws JsonProcessingException {

        LOGGER.info("===============>根据projectId={}删除数据集", projectId);
        boolean deleteResult = manageProjectDataApiService.deleteManageProjectDataByProjectId(projectId);
        ObjectMapper objectMapper = new ObjectMapper();
        if (deleteResult){
            LOGGER.info("===============>根据projectId={}删除数据集成功.", projectId);
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>根据projectId={}删除数据集失败.", projectId);
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }

    /**
     * 根据项目和数据关联记录id删除记录
     * @param id 项目和数据关联记录id
     * @return String
     */
    @GetMapping(value = "delete/{id}")
    public String deleteManageProjectDataById(@PathVariable("id") int id) throws JsonProcessingException {

        LOGGER.info("===============>根据id={}删除数据", id);
        boolean deleteResult = manageProjectDataApiService.deleteManageProjectDataById(id);
        ObjectMapper objectMapper = new ObjectMapper();
        if (deleteResult){
            LOGGER.info("===============>根据id={}删除数据成功.", id);
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>根据id={}删除数据失败.", id);
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }
}
