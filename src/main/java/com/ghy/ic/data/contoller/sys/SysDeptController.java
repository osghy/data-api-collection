package com.ghy.ic.data.contoller.sys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.form.SearchForm;
import com.ghy.ic.data.iface.sys.SysDeptApiService;
import info.center.data.sys.contract.model.SysDeptModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

//声明对外的api
//
@RestController
@RequestMapping("/sys/dept")
public class SysDeptController {

    private static final Logger logger = LoggerFactory.getLogger(SysDeptController.class);
    @Autowired
    private SysDeptApiService sysDeptApiService;

    @RequestMapping("/list")
    public String queryDeptList(@ModelAttribute SearchForm form) {
        SysDeptModel model = new SysDeptModel();
        List<SysDeptModel> list = sysDeptApiService.queryDepts(model);
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("==============>dept:{}", mapper.writeValueAsString(list));
            return mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            logger.error("=========>error:{}", e);
        }
        return null;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody String param) throws IOException {
        logger.info("===============>添加部门:{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        SysDeptModel model = objectMapper.readValue(param, SysDeptModel.class);
        model.setUpdateTime(new Date());
        model.setSyncFlag(1);
        int insertCount = sysDeptApiService.addSysDept(model);
        return String.valueOf(insertCount);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestBody String param) throws IOException {
        logger.info("===============>更新部门:{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        SysDeptModel model = objectMapper.readValue(param, SysDeptModel.class);
        int insertCount = sysDeptApiService.updateDeptById(model);
        return String.valueOf(insertCount);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@RequestBody String param) throws IOException {
        logger.info("===============>删除部门:{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, ArrayList<Integer>> map = objectMapper.readValue(param.toString(), Map.class);
        Boolean bool = sysDeptApiService.deleteSysDeptByIds(map.get("delList"));
        logger.info("===============>删除成功:{}", param);
        return String.valueOf(bool);
    }
}
