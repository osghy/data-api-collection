package com.ghy.ic.data.contoller.sys;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.vo.ResultBean;
import com.ghy.ic.data.iface.sys.SysDeptApiService;
import com.ghy.ic.data.iface.sys.SysMenuApiService;
import com.ghy.ic.data.iface.sys.SysUserApiService;
import com.ghy.ic.data.contoller.rule.RuleEngine;
import com.ghy.ic.data.iface.sys.SysUserMenuApiService;
import com.ghy.ic.data.util.excel.ExcelUtil;
import com.ghy.ic.data.verify.VerifyUtil;
import info.center.data.sys.contract.model.SysDeptModel;
import info.center.data.sys.contract.model.SysUserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * 接收导入Excel 模板并实现用户同步处理
 */
@RestController
@RequestMapping("/sys/excel")
public class SysUserExcelController {

    @Autowired
    private SysUserApiService sysUserApiService;
    @Autowired
    private SysDeptApiService sysDeptApiService;

    @Autowired
    private SysMenuApiService sysMenuApiService;

    @Autowired
    private SysUserMenuApiService sysUserMenuApiService;


    private static final Logger logger = LoggerFactory.getLogger(SysUserExcelController.class);

    @RequestMapping(value = "/import", method = {RequestMethod.GET, RequestMethod.POST})
    public String importResult(@RequestParam("file") MultipartFile file) throws Exception {
        logger.info("=====>用户开始上传操作");
        ResultBean resultBean = new ResultBean();
        String fileName = file.getOriginalFilename();
        ObjectMapper objectMapper = new ObjectMapper();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        if (VerifyUtil.verifyType(suffix, "xls") == false && VerifyUtil.verifyType(suffix, "xlsx") == false) {
            logger.info("=====>上传文件格式不正确,{},fileName:{}", suffix, fileName);
            resultBean.setMsg("上传文件格式不正确");
            resultBean.setCode(-1);
        } else {
            // read excel stream
            logger.info("=====>导入成功:{},文件名:{}", suffix, fileName);
            List<List<Object>> lists = ExcelUtil.readExcelFile(file.getInputStream(), fileName);
            logger.info("=====>导入成功:{}", lists.size());

            // 用户写入表
            List<SysUserModel> userModelList = new ArrayList<>();
            // 全部用户
            List<SysUserModel> userList = sysUserApiService.queryUsers(new SysUserModel());
            // 重复用户
            List<SysUserModel> repeatUserList=null;
            // 获取部门表
            List<SysDeptModel> deptList = sysDeptApiService.queryDepts(new SysDeptModel());
            Map<String, SysUserModel> userMatcher = new HashMap<>();
            userList.forEach(sysUserModel -> {
                userMatcher.put(sysUserModel.getLoginName(), sysUserModel);
            });
            Map<String, Integer> deptMatcher = new HashMap<>();
            deptList.forEach(sysDeptModel -> {
                deptMatcher.put(sysDeptModel.getDeptName(), sysDeptModel.getId());
            });
            List<SysDeptModel> deptAddList = new ArrayList<>();
            try{
                lists.forEach(objects -> {
                    Object deptId = deptMatcher.get(objects.get(3) + "");
                    if (null == deptId) {
                        SysDeptModel sysDeptModel = new SysDeptModel();
                        sysDeptModel.setDeptName(objects.get(3) + "");
                        sysDeptModel.setDeptFullName(objects.get(3) + "");
                        sysDeptModel.setUpdateTime(new Date());
                        sysDeptModel.setSyncFlag(0);
                        deptAddList.add(sysDeptModel);
                    }
                });
                if (deptAddList.size() > 0) {
                    sysDeptApiService.addSysDeptBatch(deptAddList);
                    deptList = sysDeptApiService.queryDepts(new SysDeptModel());
                    deptList.forEach(sysDeptModel -> {
                        deptMatcher.put(sysDeptModel.getDeptName(), sysDeptModel.getId());
                    });
                }
                lists.forEach(objects -> {
                    Object userId = userMatcher.get(objects.get(1) + "");
                    if (null == userId) {
                        SysUserModel sysUserModel = new SysUserModel();
                        sysUserModel.setUserName(objects.get(0) + "");
                        sysUserModel.setLoginName(objects.get(1) + "");
                        sysUserModel.setPassword(objects.get(2) + "");
                        sysUserModel.setDeptName(objects.get(3) + "");
                        sysUserModel.setDeptId(deptMatcher.get(objects.get(3) + ""));
                        sysUserModel.setUpdateTime(new Date());
                        sysUserModel.setSyncFlag(0);
                        sysUserModel.setLevel(objects.get(5) + "");
                        userModelList.add(sysUserModel);
                    }else{
                        //存入已存在用户。
                        repeatUserList.add(userMatcher.get(objects.get(1) + ""));
                    }
                });
                if (userModelList.size() > 0) {
                    sysUserApiService.addSysUserBatch(userModelList);
                    //  用户批量入库成功,同步用户规则执行
                    //  菜单规则--判定用户的身份(no), common rule
                    // 从容器中拿Controller，不要自己new
                    RuleEngine re=new RuleEngine();
                    logger.info("=====>运行规则:{}", userModelList);
                    re.syncUser(userModelList,sysUserApiService,sysMenuApiService,sysUserMenuApiService);
                    logger.info("=====>运行规则结束");
                }
                if(repeatUserList.size()>0){
                    resultBean.setMsg( JSONArray.toJSONString(repeatUserList));
                    resultBean.setCode(1);
                }else{
                    resultBean.setMsg("导入成功");
                    resultBean.setCode(0);
                }
            }catch(Exception e){
                logger.info("====导入异常:{}",e.getMessage());
                resultBean.setMsg("异常");
                resultBean.setCode(-2);
            }

        }
        return objectMapper.writeValueAsString(resultBean);
    }

    @RequestMapping(value = "/export", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void fileDownLoad(HttpServletResponse response) throws Exception {
        logger.info("=====>用户开始进行下载操作");
        String fileName = "users-info.xlsx";
        ObjectMapper objectMapper = new ObjectMapper();
       try (InputStream in = SysUserExcelController.class.getResourceAsStream("/templates/" + fileName)) {
            fileName = new String(fileName.getBytes("gb2312"), "iso8859-1");//防止中文乱码
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.setContentType("application/octet-stream");
            response.setStatus(200);
            OutputStream outputStream = response.getOutputStream();
            int b = 0;
            byte[] buffer = new byte[1000000];
            while (b != -1) {
                b = in.read(buffer);
                if(b!=-1) outputStream.write(buffer, 0, b);
            }
           in.close();
            outputStream.flush();
            outputStream.close();
            logger.info("=====>用户下载返回数据");
        } catch (Exception e) {
            logger.info("=====>用户下载返回异常{}", e.getMessage());
        }
    }
}
