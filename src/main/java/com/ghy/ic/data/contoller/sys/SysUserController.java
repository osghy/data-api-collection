package com.ghy.ic.data.contoller.sys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.form.SearchForm;
import com.ghy.ic.data.domain.vo.UserView;
import com.ghy.ic.data.iface.sys.*;
import com.ghy.ic.data.util.date.DateUtil;
import info.center.data.sys.contract.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

//声明对外的api
//
@RestController
@RequestMapping("/sys/user")
public class SysUserController {

    private static final Logger logger = LoggerFactory.getLogger(SysUserController.class);
    @Autowired
    private SysUserApiService sysUserApiService;
    @Autowired
    private SysDeptApiService sysDeptApiService;
    @Autowired
    private SysUserMenuApiService sysUserMenuApiService;
    @Autowired
    private SysUserRoleApiService sysUserRoleApiService;
    @Autowired
    private SysMenuApiService sysMenuApiService;

    @RequestMapping("/listAll")
    public String queryUserList(@ModelAttribute SearchForm form) {
        SysUserModel model = new SysUserModel();
        List<SysUserModel> list = sysUserApiService.queryUsers(model);
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("==============>user:{}", mapper.writeValueAsString(list));
            return mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logger.error("=========>error:{}", e);
        }
        return null;
    }

    // 查询菜单
    @RequestMapping(value = "/menuConf", method = RequestMethod.POST)
    public String queryUserMenu(@RequestBody String userId) {
        SysUserMenuModel sysUserMenuModel = new SysUserMenuModel();
        sysUserMenuModel.setUserId(Integer.parseInt(userId));
        List<SysUserMenuModel> list = sysUserMenuApiService.queryUserMenu(sysUserMenuModel);
        List<Integer> resList = new ArrayList();
        List<Integer> nodeList = new ArrayList<>();
        List<SysMenuModel> nodeModel = sysMenuApiService.queryMenuNode();
        list.forEach(menuModel -> {
            SysMenuModel sysMenuModel = new SysMenuModel();
            sysMenuModel.setId(menuModel.getMenuId());
            resList.add(menuModel.getMenuId());
        });
        nodeModel.forEach(sysMenuModel -> {
            nodeList.add(sysMenuModel.getId());
        });
        List<Integer> expandList = nodeList;
        nodeList.retainAll(resList);
        expandList.retainAll(nodeList);
        List<List<Integer>> viewList = new ArrayList<>();
        viewList.add(resList);
        viewList.add(nodeList);
        viewList.add(expandList);
        ObjectMapper mapper = new ObjectMapper();
        try {
            logger.info("==============>获取用户菜单--用户名:{},结果:{}", mapper.writeValueAsString(userId), resList);
            logger.info("==============>获取用户菜单--用户名:{},展示结果:{}", mapper.writeValueAsString(userId), nodeList);
            return mapper.writeValueAsString(viewList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logger.error("=========>error:{}", e);
        }
        return null;
    }

    // 查询角色
    @RequestMapping(value = "/roleConf", method = RequestMethod.POST)
    public String queryRoleMenu(@RequestBody String userId) {
        SysUserRoleModel model = new SysUserRoleModel();
        model.setUserId(Integer.parseInt(userId));
        List<SysUserRoleModel> list = sysUserRoleApiService.querySysUserRoleList(model);
        ObjectMapper mapper = new ObjectMapper();
        List<String> ResList = new ArrayList();
        for (SysUserRoleModel roleModel : list) {
            ResList.add(roleModel.getRoleId() + "");
        }
        try {
            logger.info("==============>获取用户角色--用户名:{},结果:{}", mapper.writeValueAsString(userId), ResList);
            if (ResList.size() > 0) {
                return mapper.writeValueAsString(ResList.get(0));
            } else {
                return mapper.writeValueAsString(ResList);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logger.error("=========>error:{}", e);
        }
        return null;
    }

    @RequestMapping(value = "/deptOptions", method = RequestMethod.POST)
    public String deptOptions(@RequestBody String param) throws IOException {
        logger.info("===============>添加部门:{}", param);

        // sysDeptApiService.queryDeptOptions();
        return null;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody String param) throws IOException {
        logger.info("===============>添加用户:{}", param);
        SysUserModel model=getSysUserModel(param);
        SysUserModel queryModel=new SysUserModel();
        queryModel.setLoginName(model.getLoginName());
        List<SysUserModel> list=sysUserApiService.queryUsers(queryModel);
        Object value;
        if (list.size()>0) {
            value="已有登录名";
        }else{
            // true false
            value= sysUserApiService.addSysUser(model);
        }
        logger.info("===============>添加用户结束:{}",value);
        return String.valueOf(value);
    }

    private SysUserModel getSysUserModel(String param) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        UserView userView = objectMapper.readValue(param, UserView.class);
        SysUserModel model = new SysUserModel();
        if(userView.getId() !=null){
            model.setId(userView.getId());
        }
        model.setLoginName(userView.getLoginName());
        model.setUserName(userView.getUserName());
        model.setDeptId(Integer.parseInt(userView.getDeptMap().split(",")[0]));
        model.setDeptName(userView.getDeptMap().split(",")[1]);
        model.setLevel(userView.getLevel());
        model.setUpdateTime(new Date());
        model.setPassword(userView.getPassword());
        String[] menuConf = userView.getMenuConf();
        List<SysUserMenuModel> userMenuModelList = new ArrayList<>();
        Date startTime = new Date();
        Date endTime = DateUtil.addDate(startTime, 30);
        userMenuModelList = setMenuListByConf(menuConf, startTime, endTime);
        model.setUserMenuModelList(userMenuModelList);
        List<SysUserRoleModel> sysUserRoleModelList = new ArrayList<>();
        SysUserRoleModel sysUserRoleModel = new SysUserRoleModel();
        sysUserRoleModel.setRoleId(Integer.parseInt(userView.getRoleConf()));
        sysUserRoleModel.setDeptId(Integer.parseInt(userView.getDeptMap().split(",")[0]));
        sysUserRoleModel.setOperator("hecore");
        sysUserRoleModelList.add(sysUserRoleModel);
        model.setUserRoleModelList(sysUserRoleModelList);
        return model;
    }

    private List<SysUserMenuModel> setMenuListByConf(String[] menuConf, Date startTime, Date endTime) {
        List<SysUserMenuModel> userMenuModelList = new ArrayList<>();
        for (String conf : menuConf) {
            SysUserMenuModel sysUserMenuModel = new SysUserMenuModel();
            sysUserMenuModel.setMenuId(Integer.parseInt(conf));
            sysUserMenuModel.setVisiableStart(startTime);
            sysUserMenuModel.setVisiableEnd(endTime);
            sysUserMenuModel.setIsvisiable(0);
            sysUserMenuModel.setUpdateTime(new Date());
            sysUserMenuModel.setOperator("hecore");
            userMenuModelList.add(sysUserMenuModel);
        }
        return userMenuModelList;
    }

    @RequestMapping(value = "/dept/treeByUserId/{userId}")
    public String deptTreeByUserId(@PathVariable int userId) throws JsonProcessingException{
        List<Object> deptTreeList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        SysDeptModel sysDeptParam = new SysDeptModel();
        SysUserModel sysUserModel=new SysUserModel();
        sysUserModel.setId(userId);
        List<SysUserModel> userModelList =sysUserApiService.queryUsers(sysUserModel);
        int deptId=userModelList.get(0).getDeptId();
        sysDeptParam.setId(deptId);
        List<SysDeptModel> deptListResult = sysDeptApiService.queryDepts(sysDeptParam);
        setDeptList(deptListResult,deptTreeList);
        return objectMapper.writeValueAsString(deptTreeList);
    }

    public void setDeptList(List<SysDeptModel> deptListResult,List<Object> deptTreeList){
        SysUserModel sysUserParam = new SysUserModel();
        List<SysUserModel> userListResult = sysUserApiService.queryUsers(sysUserParam);
        deptListResult.forEach(index -> {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("key", "dept_" + index.getId());
            map.put("value", index.getDeptName());
            List<Map<String, Object>> childList = new ArrayList<>();
            userListResult.forEach(item -> {
                if (item.getDeptId() == index.getId()) {
                    Map<String, Object> userMap = new LinkedHashMap<>();
                    userMap.put("key", String.valueOf(item.getId()));
                    userMap.put("value", item.getUserName());
                    childList.add(userMap);
                }
            });
            map.put("children", childList);
            deptTreeList.add(map);
        });
    }

    @RequestMapping(value = "/dept/tree")
    public String deptTree() throws JsonProcessingException {
        List<Object> deptTreeList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        SysDeptModel sysDeptParam = new SysDeptModel();
        List<SysDeptModel> deptListResult = sysDeptApiService.queryDepts(sysDeptParam);
        setDeptList(deptListResult,deptTreeList);
        return objectMapper.writeValueAsString(deptTreeList);
    }

    @RequestMapping(value = "/list/{deptId}", method = RequestMethod.GET)
    public String queryUserListByDeptId(@PathVariable String deptId) {
        logger.info("===============>查找部门用户开始:{}", deptId);
        if ("0".equals(deptId)) {
            SysUserModel model = new SysUserModel();
            List<SysUserModel> list = sysUserApiService.queryUsers(model);
            ObjectMapper mapper = new ObjectMapper();
            try {
                logger.info("==============>userList:{}", mapper.writeValueAsString(list));
                return mapper.writeValueAsString(list);
            } catch (JsonProcessingException e) {
                logger.error("=========>error:{}", e);
            }
        } else {
            ObjectMapper objectMapper = new ObjectMapper();
            List<SysUserModel> list = sysUserApiService.findUsersByDeptId(Integer.parseInt(deptId));
            ObjectMapper mapper = new ObjectMapper();
            try {
                logger.info("==============>listByDeptId:{}", mapper.writeValueAsString(list));
                return mapper.writeValueAsString(list);
            } catch (JsonProcessingException e) {
                logger.error("=========>error:{}", e);
            }
            logger.info("===============>查找部门用户结束:{}", deptId);
        }
        return null;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestBody String param) throws IOException {
        // {"id":66,"loginName":"ceshihyunxi","userName":"h云栖测试","password":"123456","deptMap":"2,城垣","level":"普通员工","menuConf":[53,54],"roleConf":"12"
        logger.info("===============>更新用户:{}", param);
        SysUserModel model=getSysUserModel(param);
        boolean bool = sysUserApiService.updateUserById(model);
        logger.info("===============>更新用户结束:{}", bool);
        return String.valueOf(bool);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(@RequestBody String param) throws IOException {
        logger.info("===============>删除用户:{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, ArrayList<Integer>> map = objectMapper.readValue(param.toString(), Map.class);
        Boolean bool = sysUserApiService.deleteSysUserByIds(map.get("delList"));
        logger.info("===============>删除成功:{}", param);
        return String.valueOf(bool);
    }
}


