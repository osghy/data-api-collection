package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ghy.ic.data.iface.managedata.ManageDataAuthApiService;
import com.ghy.ic.data.iface.sys.SysUserApiService;
import com.ghy.ic.data.transfer.JsonTransfer;
import com.info.center.data.sys.contract.constant.AuthTypeEnum;
import com.info.center.data.sys.contract.constant.DataTypeEnum;
import com.info.center.data.sys.contract.model.ManageDataAuthModel;
import com.info.center.data.sys.contract.model.RespCommon;
import info.center.data.sys.contract.model.SysUserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2018/12/29
 */
@RestController
@RequestMapping("/data/auth")
public class ManageAuthController {
    private static final Logger logger = LoggerFactory.getLogger(ManageTagController.class);

    @Autowired
    private ManageDataAuthApiService authApiService;
    @Autowired
    private SysUserApiService userApiService;

    @RequestMapping("/userAuthList/{dataType}/{deptId}/{dataId}")
    public String userAuthList(@PathVariable int dataType, @PathVariable("deptId") int deptId, @PathVariable("dataId") int dataId) throws JsonProcessingException {

        List<ManageDataAuthModel> dataAuthModels = authApiService.queryDataAuthsByDataId(dataId);
        SysUserModel sysUserModel = new SysUserModel();
        if (deptId != 0) {
            sysUserModel.setDeptId(deptId);
        }
        List<SysUserModel> userModels = userApiService.queryUsers(sysUserModel);
        if (dataType == DataTypeEnum.PUBLIC.value()) {
            checkPublicDataAuth(userModels, dataAuthModels); // 公共数据权限
        }
        if (dataType == DataTypeEnum.DEPT.value() || dataType == DataTypeEnum.PROJECT.value() ||
                dataType == DataTypeEnum.PRIVATE.value()) {
            checkDpDataAuth(userModels, dataAuthModels); // 业务所数据和项目数据权限和我的数据
        }
        ObjectMapper mapper = new ObjectMapper();
        logger.info(mapper.writeValueAsString(userModels));
        return mapper.writeValueAsString(userModels);
    }

    /**
     * 授权
     */
    @RequestMapping("/{dataType}/{dataId}")
    public String auth(@RequestBody String param, @PathVariable("dataId") int dataId, @PathVariable("dataType") int dataType) throws IOException {
        List<ManageDataAuthModel> dataAuthModels = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        List<Map<String, Object>> list = objectMapper.readValue(param, List.class);
        for (Map<String, Object> index : list) {
            int userId = (Integer) index.get("userId");
            List<String> authTypeList = (ArrayList) index.get("authType");
            for (String item : authTypeList) {
                // 过滤公共数据的默认的查看,公共数据的查看是默认的权限无需存储
                ManageDataAuthModel model = new ManageDataAuthModel();
                if (Integer.parseInt(item) == AuthTypeEnum.USERABLE.value() &&
                        dataType == DataTypeEnum.PUBLIC.value()) {
                    //需要删除对应的之前的权限
                    List<Integer> userIds = new ArrayList<>();
                    userIds.add(userId);
                    model.setUserIds(userIds);
                    model.setDataId(dataId);
                    authApiService.deleteAuthByDataIdAndUserIds(model);
                    continue;
                }
                model.setUserId(userId);
                model.setDataId(dataId);
                model.setType(dataType);
                model.setAuthType(Integer.parseInt(item));
                model.setOperateUser("huweiqiang");
                dataAuthModels.add(model);
            }
        }
        if (dataAuthModels.isEmpty()) {
            RespCommon respCommon = new RespCommon();
            respCommon.setInvokeSuccess(true);
            respCommon.setInvokeInfo("配置权限成功.");
            return JsonTransfer.result(respCommon);
        }
        RespCommon respCommon = authApiService.addDataAuth(dataAuthModels);
        return JsonTransfer.result(respCommon);
    }

    @RequestMapping("/queryAuthOptions")
    public String queryAuthOptions() {
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayNode arrayNode = objectMapper.createArrayNode();
        for (AuthTypeEnum index : AuthTypeEnum.values()) {
            ObjectNode node = objectMapper.createObjectNode();
            node.put("value", index.value());
            node.put("displayName", index.displayName());
            arrayNode.add(node);
        }
        return arrayNode.toString();
    }

    /**
     * 查看公共数据权限
     */
    private void checkPublicDataAuth(List<SysUserModel> userModels, List<ManageDataAuthModel> dataAuthModels) {
        userModels.forEach(user -> user.setAuthDisplay("查看"));
        userModels.forEach(user ->
            dataAuthModels.forEach(auth -> {
                if (auth.getUserId() == user.getId()) {
                    String displayName = AuthTypeEnum.valueOf(auth.getAuthType()).displayName();
                    if (auth.getAuthType() == AuthTypeEnum.FORBIDDEN_LOOK.value()) {
                        user.setAuthDisplay(displayName);
                    } else {
                        user.setAuthDisplay(user.getAuthDisplay() + "/" + displayName);
                    }
                }
            })
        );
    }

    /**
     * 查看"业务所数据"和"项目组数据"
     */
    private List<SysUserModel> checkDpDataAuth(List<SysUserModel> userModels, List<ManageDataAuthModel> dataAuthModels) {
        userModels.forEach(user -> user.setAuthDisplay("禁止查看"));
        userModels.forEach(user -> {
            dataAuthModels.forEach(auth -> {
                if (auth.getUserId() == user.getId()) {
                    String displayName = AuthTypeEnum.valueOf(auth.getAuthType()).displayName();
                    if (auth.getAuthType() == AuthTypeEnum.USERABLE.value()) {
                        user.setAuthDisplay(displayName);
                    } else {
                        user.setAuthDisplay(user.getAuthDisplay() + "/" + displayName);
                    }
                }
            });
        });
        return userModels;

    }

}

