package com.ghy.ic.data.contoller.sys;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.iface.sys.SysUserRoleApiService;
import info.center.data.sys.contract.model.SysUserRoleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2018/12/14
 */
@RestController
@RequestMapping("/sys/user/role")
public class SysUserRoleController {
    private static final Logger logger = LoggerFactory.getLogger(SysUserRoleController.class);
    @Autowired
    private SysUserRoleApiService userRoleApiService;

    @RequestMapping("/checkedKeys/{roleId}")
    public String checkedKeys(@PathVariable("roleId") int roleId) throws JsonProcessingException {

        SysUserRoleModel model = new SysUserRoleModel();
        model.setRoleId(roleId);
        List<SysUserRoleModel> list = userRoleApiService.querySysUserRoleList(model);
        ObjectMapper objectMapper = new ObjectMapper();
        logger.info("========>关系表数据:{}", objectMapper.writeValueAsString(list));
        return objectMapper.writeValueAsString(list);
    }

    @RequestMapping("/updateInData")
    public String update(@RequestBody String param) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.readValue(param, Map.class);
        List<SysUserRoleModel> roleModels = UserRoleCommon.getRoleModels(map);
        SysUserRoleModel userRoleModel = new SysUserRoleModel();
        userRoleModel.setRoleId((Integer) map.get("roleId"));
        userRoleModel.setUserRoleModels(roleModels);
        boolean boo = userRoleApiService.updateUserRoleByroleId(userRoleModel);

        return "";
    }
}
