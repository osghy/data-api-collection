package com.ghy.ic.data.contoller.sys;

import info.center.data.sys.contract.model.SysUserRoleModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * create by huweiqiang on 2019/1/10
 */
public class UserRoleCommon {


    public static List<SysUserRoleModel> getRoleModels(Map<String, Object> map) {
        List<String> list = (ArrayList) map.get("auth");
        List<SysUserRoleModel> roleModels = new ArrayList<>();
        if (!list.isEmpty()) {
            List<String> userIds = list.stream().filter(index -> !index.contains("dept_")).collect(Collectors.toList());
            for (String index : userIds) {
                SysUserRoleModel model = new SysUserRoleModel();
                model.setUserId(Integer.parseInt(index));
                model.setOperator("huweiqiang");
                model.setDeptId(0);
                roleModels.add(model);
            }
        }
        return roleModels;
    }


}
