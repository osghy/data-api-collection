package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.vo.ResponseResult;
import com.ghy.ic.data.iface.managedata.ManageProjectApiService;
import com.ghy.ic.data.iface.sys.SysUserApiService;
import com.info.center.data.sys.contract.model.ManageProjectModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author Y-Tree
 * @date 18-12-28 下午6:43
 */
@RestController
@RequestMapping("/manage/project")
public class ManageProjectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageProjectController.class);

    @Autowired
    private ManageProjectApiService manageProjectApiService;
    @Autowired
    private SysUserApiService sysUserApiService;

    /**
     * 创建项目组
     * @param param 创建项目组所需的必要信息串
     * @return int
     * @throws IOException json解析异常
     */
    @PostMapping(value = "add")
    public String addManageProject(@RequestBody String param) throws IOException {

        LOGGER.info("===============>创建项目组{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        ManageProjectModel model = objectMapper.readValue(param, ManageProjectModel.class);
        model.setOperateUser(model.getUserId());
        model.setUserName(sysUserApiService.findUserById(model.getUserId()).getUserName());
        int projectId = manageProjectApiService.addManageProject(model);
        LOGGER.info("===============>成功创建项目组{}", projectId);
        ResponseResult result = new ResponseResult<>(0, "ok");
        return objectMapper.writeValueAsString(result);
    }

    /**
     * 上传文件
     */
    @RequestMapping(value = "upload/{id}")
    public void uploadFile(@RequestBody String param, @PathVariable("id") int id, MultipartFile[] files) throws IOException {

        LOGGER.info("===============>文件上传{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        ManageProjectModel model = objectMapper.readValue(param, ManageProjectModel.class);
    }

    /**
     * 根据项目id获取项目信息
     * @param id 项目id
     * @return String
     * @throws JsonProcessingException json解析异常
     */
    @GetMapping(value = "get/{id}")
    public String queryManageProjectById(@PathVariable("id") int id) throws JsonProcessingException {

        LOGGER.info("===============>根据id={}获取项目组", id);
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseResult responseResult;
        ManageProjectModel manageProjectModel = manageProjectApiService.queryManageProjectById(id);
        if (manageProjectModel == null){
            LOGGER.error("===============>根据id={}获取项目组获取结果为空.", id);
            responseResult = new ResponseResult<>(-1, "err");
        }else {
            responseResult = new ResponseResult<>(0, "ok", manageProjectModel);
            LOGGER.info("===============>根据id={}获取项目组结果成功.", id);
        }
        return objectMapper.writeValueAsString(responseResult);
    }

    /**
     * 根据用户id获取项目信息列表
     * @param userId 用户id
     * @return String
     * @throws JsonProcessingException json解析异常
     */
    @GetMapping(value = "getList/{userId}")
    public String queryManageProjectByUserId(@PathVariable("userId") int userId) throws JsonProcessingException {

        LOGGER.info("===============>根据userId={}获取项目组列表", userId);
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseResult responseResult;
        List<ManageProjectModel> manageProjectModels
                = manageProjectApiService.queryManageProjectByUserId(userId);
        if (manageProjectModels.isEmpty()){
            LOGGER.error("===============>根据userId={}获取项目组列表为空.", userId);
            responseResult = new ResponseResult(-1, "err");
        }else {
            responseResult = new ResponseResult<>(0, "ok", manageProjectModels);
            LOGGER.info("===============>根据userId={}获取项目组列表成功.", userId);
        }
        return objectMapper.writeValueAsString(responseResult);
    }

    /**
     * 更新项目组信息
     * @param param 更新项目组所需的必要信息串
     * @return String
     * @throws IOException json解析异常
     */
    @PostMapping(value = "update")
    public String updateManageProjectById(@RequestBody String param) throws IOException {

        LOGGER.info("===============>更新项目组{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        ManageProjectModel model = objectMapper.readValue(param, ManageProjectModel.class);
        boolean updateResult = manageProjectApiService.updateManageProjectById(model);
        if (updateResult){
            LOGGER.info("===============>更新成功.");
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>更新失败.");
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }

    /**
     * 根据项目id删除该项目信息
     * @param id 项目id
     * @return String
     */
    @GetMapping(value = "delete/{id}")
    public String deleteManageProjectById(@PathVariable("id") int id) throws JsonProcessingException {

        LOGGER.info("===============>删除项目组{}", id);
        ObjectMapper objectMapper = new ObjectMapper();
        boolean deleteResult = manageProjectApiService.deleteManageProjectById(id);
        if (deleteResult){
            LOGGER.info("===============>删除成功.");
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>删除失败.");
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }
}
