package com.ghy.ic.data.contoller.sys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.form.SearchForm;
import com.ghy.ic.data.iface.sys.SysMenuApiService;
import com.ghy.ic.data.util.MenuTreeUtil;
import info.center.data.sys.contract.model.SysMenuModel;
import info.center.data.sys.contract.model.SysUserMenuModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * create by huweiqiang on 2018/11/26
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {
    private static final Logger logger = LoggerFactory.getLogger(SysMenuController.class);
    @Autowired
    private SysMenuApiService sysMenuApiService;

    @RequestMapping("/list")
    public String list(@ModelAttribute SearchForm form) throws JsonProcessingException {
        SysMenuModel model = new SysMenuModel();
        List<SysMenuModel> list = sysMenuApiService.queryMenus(model);
        if (!list.isEmpty()) {
            MenuTreeUtil menuTree = new MenuTreeUtil();
            int id = topLevelMenuId(list); //一级菜单的id
            List<Object> menuList = menuTree.menuList(list, id);
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(menuList);
        }
        return "";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(@RequestBody String param) throws IOException {
        logger.info("===============>添加菜单:{}", param);
        SysMenuModel model = getAddAndUpdateModel(param);
        boolean boo = sysMenuApiService.addSysMenu(model);
        return "";
    }

    @RequestMapping("/update")
    public String update(@RequestBody String param) throws IOException {
        logger.info("===============>编辑菜单:{}", param);
        SysMenuModel model = getAddAndUpdateModel(param);
        int updateCount = sysMenuApiService.updateMenuById(model);
        return String.valueOf(updateCount);
    }

    @RequestMapping("/delete")
    public String delete(@RequestBody String param) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.readValue(param, Map.class);
        List<Integer> list = (ArrayList<Integer>) map.get("array");
        boolean boo = sysMenuApiService.deleteMenu(list);
        logger.info("===========>删除菜单结果:{}", boo);
        return "";
    }

    /**
     * 加载select选项
     */
    @RequestMapping("/options")
    public String options() throws JsonProcessingException {
        SysMenuModel model = new SysMenuModel();
        List<SysMenuModel> list = sysMenuApiService.queryMenus(model);
        if (!list.isEmpty()) {
            MenuTreeUtil menuTreeUtil = new MenuTreeUtil();
            List<Object> response = menuTreeUtil.menuList(list);
            ObjectMapper objectMapper = new ObjectMapper();
            logger.info(objectMapper.writeValueAsString(response));
            return objectMapper.writeValueAsString(response);
        }
        return "";
    }

    /**
     * 获取增加或者修改的实例
     *
     * @return
     */
    private SysMenuModel getAddAndUpdateModel(String param) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.readValue(param, Map.class);
        List<String> list = (ArrayList) map.get("auth");
        String menuForm = objectMapper.writeValueAsString(map.get("values"));
        SysMenuModel model = objectMapper.readValue(menuForm, SysMenuModel.class);
        List<Integer> parentIdList = sysMenuApiService.queryParentIdList(model.getParentId());
        List<SysUserMenuModel> userMenuModels = new ArrayList<>();
        //过滤掉部门内容，剩下用户ID
        if (!list.isEmpty()) {
            List<String> userIds = list.stream().filter(index -> !index.contains("dept_")).collect(Collectors.toList());
            for (Integer parentId : parentIdList) {
                for (String index : userIds) {
                    SysUserMenuModel userMenuModel = new SysUserMenuModel();
                    userMenuModel.setMenuId(parentId);
                    userMenuModel.setIsvisiable(0);
                    userMenuModel.setVisiableStart(new Date());
                    userMenuModel.setVisiableEnd(getAbleDate());
                    userMenuModel.setUserId(Integer.parseInt(index));
                    userMenuModel.setOperator("huweiqiang");
                    userMenuModels.add(userMenuModel);
                }
            }
        }
        model.setUserMenuModels(userMenuModels);

        return model;
    }

    /**
     * 获取顶级菜单对应的id
     */
    private int topLevelMenuId(List<SysMenuModel> list) {
        List<SysMenuModel> topList = list.stream().filter(index -> index.getParentId() == 0).collect(Collectors.toList());
        Optional<SysMenuModel> topModel = topList.stream().findFirst();
        return topModel.get().getId();
    }

    /**
     * 增加一个月
     *
     * @return
     */
    private Date getAbleDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, 1);
        return calendar.getTime();
    }


}
