package com.ghy.ic.data.contoller.managedata;

import com.google.common.collect.Lists;
import com.info.center.data.deploy.contract.iface.MapDeployService;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Y-Tree
 * @date 18-12-26 下午5:23:00
 */
public class FileManage {

    @Autowired
    private MapDeployService mapDeployService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageDataController.class);

    /**
     * 多文件压缩并下载
     * @param response 响应体对象
     * @param dirPath 源文件夹
     * @param zipFileName 压缩后zip文件名
     */
    public boolean download(HttpServletResponse response, String dirPath, String zipFileName){

        // 1. 遍历该路径下的所有文件
        List<File> fileList = getFileList(dirPath);
        // 2.1 设置压缩包的名字
        String downloadName = zipFileName + ".zip";

//        response.setHeader("Content-Disposition", "attachment;fileName=" + downloadName);
        try {
            response.setHeader("Content-Disposition", "attachment;fileName=" + new String(downloadName.getBytes("UTF-8"),"iso-8859-1"));
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("===============>下载时文件名格式转换异常：{}", e);
        }

        // 2.2 设置压缩流：直接写入response，实现边压缩边下载
        ZipOutputStream zipos = null;
        try {
            zipos = new ZipOutputStream(new BufferedOutputStream(response.getOutputStream()));
        } catch (Exception e) {
            LOGGER.error("===============>设置压缩流异常：{}", e);
            return false;
        }

        // 2.3循环将文件写入压缩流
        DataOutputStream os = null;
        for(File f : fileList){

            try (InputStream is = new FileInputStream(f)) {
                if (zipos != null){
                    zipos.putNextEntry(new ZipEntry(f.getName()));
                    os = new DataOutputStream(zipos);
                    byte[] buffer = new byte[1024];
                    int read;
                    while ((read = is.read(buffer)) != -1) {
                        os.write(buffer, 0, read);
                    }
                    zipos.setEncoding("gbk");
                    zipos.closeEntry();
                }
            } catch (IOException e) {
                LOGGER.error("===============>下载压缩文件失败：{}", e);
                return false;
            }
        }

        try {
            if (os != null){
                os.flush();
                os.close();
            }
            if (zipos != null){
                zipos.close();
            }
        } catch (IOException e) {
            LOGGER.error("===============>关闭流失败：{}", e);
            return false;
        }

        LOGGER.info("===============>{}.zip 压缩文件成功", zipFileName);
        return true;
    }

    /**
     * 获取文件夹下所有文件
     * @param dirPath 源文件夹目录
     * @return List<File>
     */
    private List<File> getFileList(String dirPath){

        File file = new File(dirPath);
        ArrayList<File> fileList = Lists.newArrayList();
        if (file.exists()){
            File[] files = file.listFiles();
            if (files == null || files.length == 0){
                return fileList;
            }else {
                for (File f : files){
                    if (f.isDirectory()){
                        if (f.getPath().indexOf("mxd") != -1 || f.getPath().indexOf("png") != -1 ){
                            continue;
                        }
                        getFileList(f.getAbsolutePath());
                    }else {
                        if (f.getName().indexOf(".bat") != -1 || f.getName().indexOf(".sql") != -1 || f.getName().indexOf(".ini") != -1){
                            continue;
                        }
                        LOGGER.info(f.getName());
                        fileList.add(f);
                    }
                }
            }
        }

        return fileList;
    }
}
