package com.ghy.ic.data.contoller.sys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.iface.sys.SysRoleApiService;
import info.center.data.sys.contract.model.SysRoleModel;
import info.center.data.sys.contract.model.SysUserRoleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2018/12/7
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {
    private static final Logger logger = LoggerFactory.getLogger(SysRoleController.class);

    @Autowired
    private SysRoleApiService roleApiService;

    @RequestMapping("/list")
    public String list() throws JsonProcessingException {
        SysRoleModel model = new SysRoleModel();
        List<SysRoleModel> list = roleApiService.queryRoleList(model);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(list);
    }

    @RequestMapping("/add")
    public String add(@RequestBody String param) throws IOException {
        SysRoleModel model = getAddAndUpdateModel(param);
        roleApiService.addSysRole(model);
        return "";
    }

    @RequestMapping("/update")
    public String update(@RequestBody String param) throws IOException {
        SysRoleModel model = getAddAndUpdateModel(param);
        boolean boo = roleApiService.updateSysRoleById(model);
        return "";
    }

    @RequestMapping("/delete")
    public String delete(@RequestBody String param) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, ArrayList<Integer>> map = objectMapper.readValue(param, Map.class);
        boolean boo = roleApiService.deleteRoleBatch(map.get("array"));
        return "";
    }

    /**
     * 添加角色或者修改是组装参数
     *
     * @param param
     * @return
     */
    private SysRoleModel getAddAndUpdateModel(String param) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.readValue(param, Map.class);
        List<SysUserRoleModel> roleModels = UserRoleCommon.getRoleModels(map);
        String roleForm = objectMapper.writeValueAsString(map.get("values"));
        SysRoleModel model = objectMapper.readValue(roleForm, SysRoleModel.class);
        model.setUserRoleModels(roleModels);
        return model;
    }

}
