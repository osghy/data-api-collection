package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ghy.ic.data.iface.managedata.ManageTagApiService;
import com.ghy.ic.data.transfer.JsonTransfer;
import com.info.center.data.sys.contract.constant.TagTypeEnum;
import com.info.center.data.sys.contract.model.ManageTagModel;
import com.info.center.data.sys.contract.model.RespCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * create by huweiqiang on 2018/12/26
 */
@RestController
@RequestMapping("/data/tag")
public class ManageTagController {
    private static final Logger logger = LoggerFactory.getLogger(ManageTagController.class);

    @Autowired
    private ManageTagApiService manageTagApiService;

    @RequestMapping("/list")
    public String queryTags() throws JsonProcessingException {
        ManageTagModel model = new ManageTagModel();
        List<ManageTagModel> list = manageTagApiService.queryManageTags(model);
        List<ManageTagModel> dateModeList = list.stream().filter(index -> index.getType() == TagTypeEnum.DATE.value()).collect(Collectors.toList());
        Map<Integer, String> dateMap = dateModeList.stream().collect(Collectors.toMap(ManageTagModel::getId, ManageTagModel::getTagName));
        List<ManageTagModel> subjectModeList = list.stream().filter(index -> index.getType() == TagTypeEnum.SUBJECT.value()).collect(Collectors.toList());
        Map<Integer, String> subjectMap = subjectModeList.stream().collect(Collectors.toMap(ManageTagModel::getId, ManageTagModel::getTagName));
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode node = objectMapper.createObjectNode();
        node.put("date", objectMapper.writeValueAsString(dateMap));
        node.put("subject", objectMapper.writeValueAsString(subjectMap));
        logger.info("=======>标签:{}", node.toString());
        return node.toString();
    }

    @RequestMapping("/add/{tagName}")
    public String add(@PathVariable String tagName) {
        ManageTagModel model = new ManageTagModel();
        model.setType(TagTypeEnum.SUBJECT.value());
        model.setTagName(tagName);
        RespCommon respCommon = manageTagApiService.addManageTag(model);
        return JsonTransfer.result(respCommon);
    }

}
