package com.ghy.ic.data.contoller.rule;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.iface.sys.SysMenuApiService;
import com.ghy.ic.data.iface.sys.SysUserApiService;
import com.ghy.ic.data.iface.sys.SysUserMenuApiService;
import com.sun.javafx.scene.control.skin.LabeledImpl;
import info.center.data.sys.contract.enums.DefaultMenuEnum;
import info.center.data.sys.contract.model.SysMenuModel;
import info.center.data.sys.contract.model.SysUserMenuModel;
import info.center.data.sys.contract.model.SysUserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/ruleEngine")
public class RuleEngine {

    // 定时器
    @Autowired
    private SysMenuApiService sysMenuApiService; // null

    @Autowired
    private SysUserApiService sysUserApiService;

    @RequestMapping(value = "/getService")
    public   String  getService() throws JsonProcessingException {
        List<SysMenuModel> list =   sysMenuApiService.queryMenuNode();
        ObjectMapper objectMapper = new ObjectMapper();
        return  objectMapper.writeValueAsString(list);
    }

    @RequestMapping(value = "/getRes")
    public   String  getRes() throws JsonProcessingException {
//        List<String> names = new ArrayList<String>();
//        names.add("hejie");
//        names.add("xingp");
//        List list =   sysUserApiService.queryUsersByLoginNames( names);
        ObjectMapper objectMapper = new ObjectMapper();
        SysMenuModel menuModel=new SysMenuModel();
        menuModel.setMenuName(DefaultMenuEnum.MANAGE_DATA.getDisplayName());
        List<SysMenuModel> menuList=sysMenuApiService.queryMenus(menuModel);
        SysMenuModel model=new SysMenuModel();
        model.setParentId(menuList.get(0).getId());
        List list=sysMenuApiService.queryMenus(model);
        //List<Integer> list = sysMenuApiService.queryParentIdList(model.getParentId());
        return  objectMapper.writeValueAsString(list);
    }


    /**
     * @param userModelList
     * @return
     * @throws Exception
     */
    public boolean syncUser(List<SysUserModel> userModelList, SysUserApiService sysUserApiService,SysMenuApiService sysMenuApiService,SysUserMenuApiService sysUserMenuApiService) throws Exception{
        boolean flag=false;
        // syncMenu
        List resList=userModelList.stream().map(SysUserModel::getLoginName).collect(Collectors.toList());
        List<SysUserModel> userList=sysUserApiService.queryUsersByLoginNames(resList);
        // 含有userId的List.
        syncMenu(userList,sysMenuApiService,sysUserMenuApiService,"Excel同步");
        return flag;
    }

    private void syncMenu(List<SysUserModel> userModelList, SysMenuApiService sysMenuApiService,SysUserMenuApiService sysUserMenuApiService,String Operator) {
        //
        SysMenuModel menuModel=new SysMenuModel();
        menuModel.setMenuName(DefaultMenuEnum.MANAGE_DATA.getDisplayName());
        List<SysMenuModel> menuList=sysMenuApiService.queryMenus(menuModel);
        if(null==menuList||menuList.size()==0) {
            throw new RuntimeException("未查到 名为'数据管理'的菜单");
        }
        // set Menu Deal
        SysMenuModel model=new SysMenuModel();
        model.setParentId(menuList.get(0).getId());
        // 查询 逻辑.
        List<Integer> subMenuList =sysMenuApiService.queryMenus(model).stream().filter(SysMenuModel->!SysMenuModel.getMenuName().equals("我的数据")).map(SysMenuModel::getId).collect(Collectors.toList());
        //sysMenuApiService.queryParentIdList(model.getParentId());
        subMenuList.add(menuList.get(0).getId());
        subMenuList.add(menuList.get(0).getParentId());
        // 过滤取消


        List<SysUserMenuModel> userMenuModels = new ArrayList<>();
        //过滤掉部门内容，剩下用户ID
        if (!userModelList.isEmpty()) {
            List<Integer> userIds =userModelList.stream().map(SysUserModel::getId).collect(Collectors.toList());
            for (Integer parentId :  subMenuList) {
                for (int index : userIds) {
                    SysUserMenuModel userMenuModel = new SysUserMenuModel();
                    userMenuModel.setMenuId(parentId);
                    userMenuModel.setIsvisiable(0);
                    userMenuModel.setVisiableStart(new Date());
                    userMenuModel.setVisiableEnd(getAbleDate());
                    userMenuModel.setUserId(index);
                    userMenuModel.setOperator(Operator);
                    userMenuModels.add(userMenuModel);
                }
            }
        }
        model.setUserMenuModels(userMenuModels);
        sysUserMenuApiService.addUserMenuBatch(userMenuModels);
        //sysMenuApiService.addSysMenu(model);
    }

    /**
     * 增加一个月
     *
     * @return
     */
    private Date getAbleDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, 1);
        return calendar.getTime();
    }

}
