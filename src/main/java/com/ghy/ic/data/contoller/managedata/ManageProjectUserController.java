package com.ghy.ic.data.contoller.managedata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.domain.vo.ResponseResult;
import com.info.center.data.sys.contract.iface.ManageProjectUserService;
import com.info.center.data.sys.contract.model.ManageProjectUserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Y-Tree
 * @date 19-1-2 下午4:21
 */
@RestController
@RequestMapping("/manage/project/user")
public class ManageProjectUserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageProjectUserController.class);

    private static final String PROJECT_ID = "projectId";
    private static final String USERS = "users";

    @Autowired
    private ManageProjectUserService manageProjectUserService;

    /**
     * 添加用户集
     * @param param 用户集信息字符串 {"projectId":[1], "users":[1,2,3,4,5]}
     * @return String
     */
    @PostMapping(value = "add")
    public String addManageProjectUsers(@RequestBody String param) throws IOException {

        LOGGER.info("===============>添加用户集{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, List<Integer>> map = objectMapper.readValue(param, Map.class);
        Integer projectId = map.get(PROJECT_ID).get(0);
        List<Integer> users = map.get(USERS);
        int insertCount = manageProjectUserService.addManageProjectUsers(projectId, users);
        if (insertCount <= 0){
            LOGGER.error("===============>添加用户集失败.");
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        LOGGER.info("===============>添加用户集成功.");
        return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
    }

    /**
     * 删除用户
     * @param param 用户集信息字符串{"projectId":[1], "users":[1,2,3,4,5]}
     * @return String
     */
    @PostMapping(value = "delete")
    public String deleteManageProjectUsers(@RequestBody String param) throws IOException {

        LOGGER.info("===============>删除用户集{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, List<Integer>> map = objectMapper.readValue(param, Map.class);
        Integer projectId = map.get(PROJECT_ID).get(0);
        List<Integer> users = map.get(USERS);
        boolean deleteResult = manageProjectUserService.deleteManageProjectUsers(projectId, users);
        if (deleteResult){
            LOGGER.info("===============>删除用户集成功.");
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>删除用户集失败.");
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }

    /**
     * 添加用户列表管理权限
     * @param param 用户集信息字符串{"projectId":[1], "users":[1,2,3,4,5]}
     * @return String
     */
    @PostMapping(value = "addRole")
    public String addRoleManageProjectUsers(@RequestBody String param) throws IOException {

        LOGGER.info("===============>添加用户权限{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, List<Integer>> map = objectMapper.readValue(param, Map.class);
        Integer projectId = map.get(PROJECT_ID).get(0);
        List<Integer> users = map.get(USERS);
        boolean addResult = manageProjectUserService.addRoleManageProjectUsers(projectId, users);
        if (addResult){
            LOGGER.info("===============>添加用户权限成功.");
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>添加用户权限失败.");
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }

    /**
     * 删除用户列表管理权限
     * @param param 用户集信息字符串{"projectId":[1], "users":[1,2,3,4,5]}
     * @return String
     */
    @PostMapping(value = "deleteRole")
    public String deleteRoleManageProjectUsers(@RequestBody String param) throws IOException {

        LOGGER.info("===============>删除用户权限{}", param);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, List<Integer>> map = objectMapper.readValue(param, Map.class);
        Integer projectId = map.get(PROJECT_ID).get(0);
        List<Integer> users = map.get(USERS);
        boolean deleteResult = manageProjectUserService.deleteRoleManageProjectUsers(projectId, users);
        if (deleteResult){
            LOGGER.info("===============>删除用户权限成功.");
            return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok"));
        }
        LOGGER.error("===============>删除用户权限失败.");
        return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
    }

    /**
     * 根据项目id获取用户列表
     * @param projectId 项目id
     * @return String
     */
    @GetMapping(value = "list/{projectId}")
    public String queryManageProjectUsers(@PathVariable("projectId") int projectId) throws JsonProcessingException {

        LOGGER.info("===============>根据projectId={}获取用户列表.", projectId);
        List<ManageProjectUserModel> models = manageProjectUserService.queryManageProjectUsers(projectId);
        ObjectMapper objectMapper = new ObjectMapper();
        if (models.isEmpty()){
            LOGGER.error("===============>根据projectId={}获取用户列表失败.", projectId);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        LOGGER.info("===============>根据projectId={}获取用户列表成功.", projectId);
        return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok", models));
    }

    /**
     * 根据用户id获取项目列表
     * @param userId 用户id
     * @return String
     */
    @GetMapping(value = "projects/{userId}")
    public String queryManageProjectsByUserId(@PathVariable("userId") int userId) throws JsonProcessingException {

        LOGGER.info("===============>根据userId={}获取用户所在项目列表.", userId);
        List<ManageProjectUserModel> models = manageProjectUserService.queryManageProjectsByUserId(userId);
        ObjectMapper objectMapper = new ObjectMapper();
        if (models.isEmpty()){
            LOGGER.error("===============>根据userId={}获取用户列表失败.", userId);
            return objectMapper.writeValueAsString(new ResponseResult<>(-1, "err"));
        }
        LOGGER.info("===============>根据projectId={}获取用户列表成功.", userId);
        return objectMapper.writeValueAsString(new ResponseResult<>(0, "ok", models));
    }
}
