package com.ghy.ic.data.contoller.sys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ghy.ic.data.iface.sys.SysMenuApiService;
import com.ghy.ic.data.iface.sys.SysRoleApiService;
import com.ghy.ic.data.iface.sys.SysUserMenuApiService;
import com.ghy.ic.data.iface.sys.SysUserRoleApiService;
import com.ghy.ic.data.util.MenuTreeUtil;
import info.center.data.sys.contract.model.SysMenuModel;
import info.center.data.sys.contract.model.SysRoleModel;
import info.center.data.sys.contract.model.SysUserMenuModel;
import info.center.data.sys.contract.model.SysUserRoleModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * create by huweiqiang on 2018/12/13
 */
@RestController
@RequestMapping("/sys/user/menu")
public class SysUserMenuController {
    private static final Logger logger = LoggerFactory.getLogger(SysUserMenuController.class);
    @Autowired
    private SysUserMenuApiService userMenuApiService;
    @Autowired
    private SysMenuApiService menuApiService;
    @Autowired
    private SysUserRoleApiService userRoleApiService;
    @Autowired
    private SysRoleApiService roleApiService;

    /**
     * 获取菜单权限的相关的人员
     *
     * @return
     */
    @RequestMapping("/checkedKeys/{menuId}")
    public String checkedKeys(@PathVariable("menuId") int menuId) throws JsonProcessingException {

        SysUserMenuModel model = new SysUserMenuModel();
        model.setMenuId(menuId);
        List<SysUserMenuModel> list = userMenuApiService.queryUserMenu(model);
        ObjectMapper objectMapper = new ObjectMapper();
        logger.info("========>关系表数据:{}", objectMapper.writeValueAsString(list));
        return objectMapper.writeValueAsString(list);
    }

    /**
     * 加载菜单
     */
    @RequestMapping("/getMenus/{userId}")
    public String getMenus(@PathVariable("userId") int userId) throws JsonProcessingException {
        List<Object> list = new ArrayList<>();
        List<SysMenuModel> menuModels = new ArrayList<>();
        List<Integer> userIdList = getAdminUserIds();
        if (!userIdList.isEmpty() && userIdList.contains(userId)) {
            menuModels.addAll(menuApiService.queryMenus(new SysMenuModel()));
        } else {
            SysUserMenuModel userMenuModel = new SysUserMenuModel();
            userMenuModel.setUserId(userId);
            List<SysUserMenuModel> userMenuModels = userMenuApiService.queryUserMenu(userMenuModel);
            if (!userMenuModels.isEmpty()) {
                List<Integer> menuIdList = userMenuModels.stream().map(SysUserMenuModel::getMenuId).collect(Collectors.toList());
                SysMenuModel sysMenuModel = new SysMenuModel();
                sysMenuModel.setIds(menuIdList);
                menuModels.addAll(menuApiService.queryMenus(sysMenuModel));
            }
        }
        MenuTreeUtil menuTreeUtil = new MenuTreeUtil();
        list = menuTreeUtil.sideMenus(menuModels);
        ObjectMapper objectMapper = new ObjectMapper();
        String res = objectMapper.writeValueAsString(list);
        logger.info(res);
        return res;
    }

    /**
     * 查看是否是超级管理员
     */
    private List<Integer> getAdminUserIds() {
        SysRoleModel roleModel = new SysRoleModel();
        roleModel.setRoleName("超级管理员");
        List<SysRoleModel> sysRoleModels = roleApiService.queryRoleList(roleModel);
        Optional<SysRoleModel> optional = sysRoleModels.stream().findFirst();
        SysRoleModel model = optional.get();
        int roleId = model.getId();
        SysUserRoleModel userRoleModel = new SysUserRoleModel();
        userRoleModel.setRoleId(roleId);
        List<SysUserRoleModel> userRoleModelList = userRoleApiService.querySysUserRoleList(userRoleModel);
        List<Integer> userIdList = userRoleModelList.stream().map(SysUserRoleModel::getUserId).collect(Collectors.toList());
        return userIdList;
    }

}
