package com.ghy.ic.data.util.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * create by huweiqiang on 2018/9/30
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SheetSetting {
    String name() default "";
    Class styleTemplate() default DefaultStyleTemplate.class;
    int headerRowIndex() default 0;
}
