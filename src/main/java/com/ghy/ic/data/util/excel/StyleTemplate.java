package com.ghy.ic.data.util.excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.HashMap;
import java.util.Map;

/**
 * create by huweiqiang on 2018/9/30
 */
public abstract class StyleTemplate {
    protected HSSFWorkbook workbook;
    protected Map<String,HSSFCellStyle> mapCache = new HashMap<>();

    public StyleTemplate(HSSFWorkbook workbook){
        this.workbook = workbook;
        init();
    }

    public HSSFCellStyle get(String styleKey){
        return mapCache.get(styleKey);
    }

    public boolean contains(String styleKey) {
        return mapCache.containsKey(styleKey);
    }

    protected abstract void init();

}
