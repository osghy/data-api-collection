package com.ghy.ic.data.util.excel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * @Title: ExcelUtil
 */
public class ExcelUtil {

    private static Logger logger = LoggerFactory.getLogger(ExcelUtil.class);


    /**
     * http请求导出excel
     *
     * @param httpRequest  httpRequest
     * @param httpResponse httpResponse
     * @param filename     excel文件名（带后缀名）
     * @param data         数据实体集合
     * @param sheetType    Excel样式实体类
     * @param <T>
     */
    public static <T extends ExcelExportedAble> void httpExportToExcel(HttpServletRequest httpRequest, HttpServletResponse httpResponse, String filename, Collection<T> data, Class<T> sheetType) {
        String agent = httpRequest.getHeader("USER-AGENT");
        String newFileName = "未命名文件.xls";
        try {
            if ((agent != null) && (agent.contains("MSIE"))) {
                newFileName = URLEncoder.encode(filename, "UTF-8");
                newFileName = StringUtils.replace(newFileName, "+", "%20");
                if (newFileName.length() > 150) {
                    newFileName = new String(filename.getBytes("GB2312"), "ISO8859-1");
                    newFileName = StringUtils.replace(newFileName, " ", "%20");
                }
            }
            if ((agent != null) && (agent.contains("Mozilla"))) {
                newFileName = new String(filename.getBytes("GB2312"), "ISO8859-1");
                newFileName = StringUtils.replace(newFileName, " ", "%20");
            }
            httpResponse.setContentType("application/vnd.ms-excel");
            httpResponse.setHeader("Content-disposition", "attachment;filename=" + newFileName);
            OutputStream outputStream = httpResponse.getOutputStream();
            ExcelExporters.export(outputStream, data, sheetType);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }


    /**
     * 读取excel文件
     *
     * @param file
     * @return
     */
    @SuppressWarnings("resource")
    public static List<List<Object>> readExcelFile(File file) {

        List<List<Object>> list = new ArrayList<List<Object>>();

        boolean isE2007 = false;    //判断是否是excel2007格式
        if (file.getName().endsWith("xlsx")) {
            isE2007 = true;
        }
        InputStream input = null;
        try {
            input = new FileInputStream(file);  //建立输入流
            Workbook wb = null;
            //根据文件格式(2003或者2007)来初始化
            if (isE2007)
                wb = new XSSFWorkbook(input);
            else
                wb = new HSSFWorkbook(input);
            Sheet sheet = wb.getSheetAt(0);     //获得第一个表单
            Iterator<Row> rows = sheet.rowIterator(); //获得第一个表单的迭代器
            while (rows.hasNext()) {

                List<Object> values = new ArrayList<Object>();
                Row row = rows.next();  //获得行数据
                if (row.getRowNum() == 0) {
                    continue;
                }
                Iterator<Cell> cells = row.cellIterator();    //获得第一行的迭代器
                while (cells.hasNext()) {
                    Cell cell = cells.next();
                    switch (cell.getCellType()) {   //根据cell中的类型来输出数据
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            dealCellTypeNumeric(values, cell);
                            break;
                        case HSSFCell.CELL_TYPE_STRING:
                            values.add(cell.getStringCellValue());
                            break;
                        case HSSFCell.CELL_TYPE_BOOLEAN:
                            values.add(cell.getBooleanCellValue());
                            break;
                        case HSSFCell.CELL_TYPE_FORMULA:
                            values.add(cell.getCellFormula());
                            break;
                        default:
                            values.add(null);
                            break;
                    }

                }
                list.add(values);
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (input != null) {
                try {
                    input.close();

                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return list;
    }


    /**
     * 读取excel文件
     *
     * @return
     */
    @SuppressWarnings("resource")
    public static List<List<Object>> readExcelFile(InputStream in, String fileName) {

        List<List<Object>> list = new ArrayList<List<Object>>();

        boolean isE2007 = false;    //判断是否是excel2007格式
        if (fileName.endsWith("xlsx"))
            isE2007 = true;
        try {
            Workbook wb = null;
            //根据文件格式(2003或者2007)来初始化
            if (isE2007)
                wb = new XSSFWorkbook(in);
            else
                wb = new HSSFWorkbook(in);
            Sheet sheet = wb.getSheetAt(0);     //获得第一个表单
            Iterator<Row> rows = sheet.rowIterator(); //获得第一个表单的迭代器
            while (rows.hasNext()) {


                Row row = rows.next();  //获得行数据
                if (row.getRowNum() == 0) {
                    continue;
                }
                List<Object> values = new ArrayList<Object>();
                Iterator<Cell> cells = row.cellIterator();    //获得第一行的迭代器
                while (cells.hasNext()) {
                    Cell cell = cells.next();
                    if (cell == null) {
                        values.add("");
                        continue;
                    }
                    switch (cell.getCellType()) {   //根据cell中的类型来输出数据
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            dealCellTypeNumeric(values, cell);
                            break;
                        case HSSFCell.CELL_TYPE_STRING:
                            values.add(cell.getStringCellValue());
                            break;
                        case HSSFCell.CELL_TYPE_BOOLEAN:
                            values.add(cell.getBooleanCellValue());
                            break;
                        case HSSFCell.CELL_TYPE_FORMULA:
                            values.add(cell.getCellFormula());
                            break;
                        case HSSFCell.CELL_TYPE_BLANK:

                        default:
                            values.add(null);
                            break;
                    }

                }
                if (!isAllNull(values)) {
                    list.add(values);
                }
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return list;
    }

    private static void dealCellTypeNumeric(List<Object> values, Cell cell) {
        if (HSSFDateUtil.isCellDateFormatted(cell)) {
            values.add(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()));
        } else {
            NumberFormat nf = NumberFormat.getInstance();
            nf.setGroupingUsed(false);//true时的格式：1,234,567,890
            values.add(nf.format(cell.getNumericCellValue()));//数值类型的数据为double，所以需要转换一下
        }
    }

    private static boolean isAllNull(List<Object> cells) {
        for (Object o : cells) {
            if (o != null) {
                return false;
            }
        }
        return true;
    }


}
