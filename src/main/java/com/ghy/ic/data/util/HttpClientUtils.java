package com.ghy.ic.data.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.util.List;

/**
 * create by huweiqiang on 2018/9/26
 */
public class HttpClientUtils {
    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);

    /**
     * post方式  get方式
     *
     * @param url
     * @return
     */
    public static JSONObject getWxJson(String url, String methodName, List<NameValuePair> list) {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        JSONObject json = null;
        HttpResponse res = null;
        try {
            if (methodName.equals(HttpPost.METHOD_NAME)) {
                HttpPost post = new HttpPost(url);
                post.setEntity(new UrlEncodedFormEntity(list, Consts.UTF_8));
                res = client.execute(post);
                HttpEntity entity = res.getEntity();
                String responseContent = EntityUtils.toString(entity, "UTF-8");
                json = JSONObject.parseObject(responseContent);
            }
            if (methodName.equals(HttpGet.METHOD_NAME)) {
                HttpGet get = new HttpGet(url);
                res = client.execute(get);
                HttpEntity entity = res.getEntity();
                String responseContent = EntityUtils.toString(entity, "UTF-8");
                json = JSONObject.parseObject(responseContent);
            }
        } catch (Exception e) {
            logger.error("=======>Exception:{}", e);
        } finally {
            try {
                client.close();
            } catch (IOException e) {
               logger.error("关闭http连接异常，e{}",e);
            }
        }
        if (res != null && res.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            logger.info("===============>请求微信接口成功");
        } else {
            logger.info("===============>请求微信接口失败");
        }
        return json;
    }


}
