package com.ghy.ic.data.util.excel;

import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * create by huweiqiang on 2018/9/30
 */
public class ExcelExPorter {

    private HSSFWorkbook hssfWorkbook;
    private OutputStream outputStream;
    private Map<Integer, EESheet> sheetMapCache = new HashMap<>();
    private AtomicInteger sheetCounter = new AtomicInteger(1);
    @Setter
    private int defaultColumnWidth = 15;

    public ExcelExPorter(OutputStream out) {
        hssfWorkbook = new HSSFWorkbook();
        this.outputStream = out;
    }
    public class EESheet<T extends ExcelExportedAble> {
        private int index;
        private HSSFSheet hssfSheet;
        private Class<T> sheetType;
        private String name;
        private List<Field> sortedFileds;
        private StyleTemplate styleTemplate;
        private int headerRowIndex;

        public int getIndex() {
            return index;
        }

        public HSSFSheet getSheet() {
            return hssfSheet;
        }

        public void setSheet(HSSFSheet sheet) {
            this.hssfSheet = sheet;
            initColumnHeader(this.hssfSheet);
        }

        public String getName() {
            return name;
        }

        public Class<T> getSheetType() {
            return sheetType;
        }
        private EESheet(int index,Class<T> sheetType){
            this.index = index;
            this.sheetType = sheetType;
            initEESheet(sheetType);
        }

        private void initColumnHeader(HSSFSheet sheet) {
            HSSFRow headerRow = sheet.createRow(headerRowIndex);
            for (int i = 0; i < sortedFileds.size(); i++) {
                HSSFCell cell = headerRow.createCell(i);
                ColumnSetting columnSetting = sortedFileds.get(i).getDeclaredAnnotation(ColumnSetting.class);
                String headCellText = sortedFileds.get(i).getName();
                HSSFCellStyle style = null;
                if(styleTemplate instanceof DefaultStyleTemplate){
                   style =  styleTemplate.get(DefaultStyleTemplate.DefaultStyle.Header.key());
                }
                if(columnSetting != null){
                    //设置表头名称
                    if(StringUtils.isNotBlank(columnSetting.name())){
                        headCellText = columnSetting.name();
                    }
                    //设置表头样式
                    if(StringUtils.isNotBlank(columnSetting.headerStyleKey())&&styleTemplate.contains(columnSetting.headerStyleKey())){
                        style = styleTemplate.get(columnSetting.headerStyleKey());
                    }
                }
                if (style != null) {
                    cell.setCellStyle(style);
                }
                cell.setCellValue(new HSSFRichTextString(headCellText));
            }
        }

        private void initEESheet(Class<T> sheetType){
            SheetSetting sheetSetting = sheetType.getAnnotation(SheetSetting.class);
            String sheetTitle = sheetType.getSimpleName();
            StyleTemplate styleTemplate1 = new DefaultStyleTemplate(hssfWorkbook);
            int headerRowIndex1 = 0;
            if(sheetSetting != null){
                //设置sheet title
                if(StringUtils.isNotBlank(sheetSetting.name())){
                    sheetTitle = sheetSetting.name();
                }
                //初始化样式模板
                if (sheetSetting.styleTemplate() != null) {
                    Class templateClazz = sheetSetting.styleTemplate();
                    if (StyleTemplate.class.isAssignableFrom(templateClazz)) {
                        try {
                            @SuppressWarnings("unchecked")
                            Constructor constructor = templateClazz.getConstructor(HSSFWorkbook.class);
                            styleTemplate1 = (StyleTemplate) constructor.newInstance(hssfWorkbook);
                        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //header行的索引
                headerRowIndex1 = sheetSetting.headerRowIndex();
            }
            this.name = sheetTitle;
            this.styleTemplate = styleTemplate1;
            this.sortedFileds = sortFields(sheetType);
            this.headerRowIndex = headerRowIndex1;
        }

        public void produceData(Collection<T> data) {
            if (!CollectionUtils.isEmpty(data)) {
                Iterator<T> it = data.iterator();
                int index = headerRowIndex;
                while (it.hasNext()) {
                    T t = it.next();
                    index++;
                    HSSFRow row = hssfSheet.createRow(index);
                    for (int i = 0; i < sortedFileds.size(); i++) {
                        Field field = sortedFileds.get(i);
                        HSSFCell cell = row.createCell(i);
                        ColumnSetting columnSetting = field.getAnnotation(ColumnSetting.class);
                        HSSFCellStyle cellStyle = null;
                        if (styleTemplate instanceof DefaultStyleTemplate) {
                            cellStyle = styleTemplate.get(DefaultStyleTemplate.DefaultStyle.Cell.key());
                        }
                        if (columnSetting != null && StringUtils.isNotBlank(columnSetting.cellStyleKey()) && styleTemplate.contains(columnSetting.cellStyleKey())) {
                            cellStyle = styleTemplate.get(columnSetting.cellStyleKey());
                        }
                        if (cellStyle != null) {
                            cell.setCellStyle(cellStyle);
                        }
                        HandlerChain.getInstance().handle(field, t, cell);
                    }
                }
            }
        }

        private List<Field> sortFields(Class<T> sheetType) {
            return Stream.of(sheetType.getDeclaredFields()).sorted(
                    (Field f1, Field f2) -> {
                        ColumnSetting header1 = f1.getAnnotation(ColumnSetting.class);
                        ColumnSetting header2 = f2.getAnnotation(ColumnSetting.class);
                        Integer order1 = Integer.MAX_VALUE;
                        Integer order2 = Integer.MAX_VALUE;
                        if (header1 != null) {
                            order1 = header1.order();
                        }
                        if (header2 != null) {
                            order2 = header2.order();
                        }
                        return order1.compareTo(order2);
                    }
            ).collect(Collectors.toList());
        }

    }
    public <T extends ExcelExportedAble> EESheet<T> createSheet(Class<T> sheetType) {
        EESheet<T> eeSheet = new EESheet<>(sheetCounter.getAndIncrement(), sheetType);
        HSSFSheet sheet = hssfWorkbook.createSheet(eeSheet.getName());
        sheet.setDefaultColumnWidth(defaultColumnWidth);
        eeSheet.setSheet(sheet);
        sheetMapCache.put(eeSheet.getIndex(), eeSheet);
        return eeSheet;
    }

    /**
     * 按照sheet实际创建顺序获取EESheet
     *
     * @param idx
     * @return
     */
    public EESheet getSheet(int idx) {
        EESheet sheet = sheetMapCache.get(idx);
        if (sheet == null) {
            throw new ExcelExporterException("请确保获取sheet前通过通过createSheet创建sheet");
        }
        return sheet;
    }

    public void export() {
        try {
            hssfWorkbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
