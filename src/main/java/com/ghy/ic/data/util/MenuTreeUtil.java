package com.ghy.ic.data.util;

import info.center.data.sys.contract.model.SysMenuModel;
import info.center.data.sys.contract.model.SysUserModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * create by huweiqiang on 2018/12/4
 */
public class MenuTreeUtil {

    private List<SysMenuModel> menuCommon;
    private List<SysUserModel> deptCommon;
    private List<Object> list = new ArrayList<>();

    public List<Object> menuList(List<SysMenuModel> menu,int id) {
        this.menuCommon = menu;
        return menuChild(id);
    }

    public List<Object> menuChild(int id) {
        List<Object> lists = new ArrayList<>();
        for (SysMenuModel a : menuCommon) {
            Map<String, Object> childArray = new LinkedHashMap<>();
            if (a.getParentId() == id) {
                childArray.put("id", a.getId());
                childArray.put("menuName", a.getMenuName());
                childArray.put("parentId", a.getParentId());
                childArray.put("remark", a.getRemark());
                childArray.put("orderNum", a.getOrderNum());
                childArray.put("url", a.getUrl());
                childArray.put("locale", a.getLocale());
                childArray.put("createTime", a.getCreateTime());
                childArray.put("key", a.getId());
                if (!menuChild(a.getId()).isEmpty()) {
                    childArray.put("children", menuChild(a.getId()));
                }
                lists.add(childArray);
            }
        }
        return lists;
    }

    public List<Object> menuList(List<SysMenuModel> menu){
        this.menuCommon = menu;
        for (SysMenuModel x : menu) {
            Map<String, Object> map = new LinkedHashMap<>();
            if(x.getParentId()==0){
                map.put("title", x.getMenuName());
                map.put("value", x.getId());
                map.put("key", x.getId());
                map.put("children", menuSelectChild(x.getId()));
                list.add(map);
            }
        }
        return list;
    }

    private List<Object> menuSelectChild(int id){

        List<Object> lists = new ArrayList<>();
        for (SysMenuModel index : menuCommon) {
            Map<String, Object> childArray = new LinkedHashMap<>();
            if (index.getParentId() == id) {
                childArray.put("title", index.getMenuName());
                childArray.put("value", index.getId());
                childArray.put("key", index.getId());
                childArray.put("children", menuSelectChild(index.getId()));
                lists.add(childArray);
            }
        }
        return lists;
    }


    public List<Object> sideMenus(List<SysMenuModel> menuModels) {
        this.menuCommon = menuModels;
        for (SysMenuModel model : menuModels) {
            if (model.getParentId() == 0) {
                list.addAll(sideChildMenus(model.getId()));
            }
        }
        return list;
    }

    private List<Object> sideChildMenus(int menuId) {
        List<Object> lists = new ArrayList<>();
        for (SysMenuModel index : menuCommon) {
            Map<String, Object> map = new LinkedHashMap<>();
            if (index.getParentId() == menuId) {
                map.put("path", index.getUrl());
                map.put("name", index.getMenuName());
                map.put("icon", index.getIcon());
                map.put("locale", index.getLocale());
                map.put("children", sideChildMenus(index.getId()));
                lists.add(map);
            }
        }
        return lists;
    }

}
