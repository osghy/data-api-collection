package com.ghy.ic.data.util.excel;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.FillPatternType;

/**
 * create by huweiqiang on 2018/9/30
 */
public class DefaultStyleTemplate extends StyleTemplate {

    protected enum DefaultStyle {
        Header("default_header"),
        Cell("default_cell");
        private String key;

        DefaultStyle(String key) {
            this.key = key;
        }

        public String key() {
            return key;
        }
    }

    public DefaultStyleTemplate(HSSFWorkbook workbook) {
        super(workbook);
    }

    @Override
    protected void init() {
        // 生成默认的header样式
        HSSFCellStyle hssfCellStyle = workbook.createCellStyle();
        //设置这些样式
        hssfCellStyle.setFillBackgroundColor(HSSFColor.SKY_BLUE.index);
        hssfCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        hssfCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 生成一个字体
        HSSFFont font = workbook.createFont();
        font.setColor(HSSFColor.VIOLET.index);
        font.setFontHeightInPoints((short) 12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        hssfCellStyle.setFont(font);
        mapCache.put(DefaultStyle.Header.key(),hssfCellStyle);

        // 生成并设置cell样式
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        // 生成另一个字体
        HSSFFont font2 = workbook.createFont();
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        // 把字体应用到当前的样式
        cellStyle.setFont(font2);
        mapCache.put(DefaultStyle.Cell.key(),cellStyle);
    }
}
