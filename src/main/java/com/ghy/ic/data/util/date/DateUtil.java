package com.ghy.ic.data.util.date;

import java.util.Date;

public class DateUtil {

    public static Date addDate(Date date, long day)  {
        long time = date.getTime(); // 得到指定日期的毫秒数
        day = day*24*60*60*1000; // 要加上的天数转换成毫秒数
        time+=day; // 相加得到新的毫秒数
        return new Date(time); // 将毫秒数转换成日期
    }
}
