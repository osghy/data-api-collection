package com.ghy.ic.data.util.excel;

/**
 * Created by huweiqiang
 */
public class ExcelExporterException extends RuntimeException {
    public ExcelExporterException(String message) {
        super(message);
    }

    public ExcelExporterException(String message, Throwable cause) {
        super(message, cause);
    }
}
