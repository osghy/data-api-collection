package com.ghy.ic.data.util.excel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by huweiqiang
 * @desc Date类型的处理器
 *       pattern通过ColumnSetting中datePattern设置
 */
public class DateHandler extends CellTypeHandler{
    private static final String DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public DateHandler() {
        super(0);
    }

    @Override
    public <T extends ExcelExportedAble> boolean handle(Field field, T obj, HSSFCell cell) {
        if (Date.class.isAssignableFrom(field.getType())) {
            try {
                PropertyDescriptor pd = new PropertyDescriptor(field.getName(),obj.getClass());
                Method getter = pd.getReadMethod();
                Date value = (Date)getter.invoke(obj);
                ColumnSetting columnSetting = field.getAnnotation(ColumnSetting.class);
                String datePattern = DEFAULT_PATTERN;
                if (columnSetting != null) {
                    datePattern = columnSetting.datePattern();
                }
                SimpleDateFormat df = new SimpleDateFormat(datePattern);
                cell.setCellValue(new HSSFRichTextString(df.format(value)));
            } catch (IllegalAccessException | IntrospectionException | InvocationTargetException e) {
                throw new ExcelExporterException(e.getMessage(),e);
            }
            return true;
        }
        return false;
    }
}
