package com.ghy.ic.data.shiro;

import com.ghy.ic.data.iface.sys.SysMenuApiService;
import info.center.data.sys.contract.model.SysUserModel;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * create by huweiqiang on 2018/11/27
 * 自定义权限匹配和账号密码匹配
 */
public class UserShiroRealm extends AuthorizingRealm {
    private static final Logger logger = LoggerFactory.getLogger(UserShiroRealm.class);
    @Autowired
    private SysMenuApiService sysMenuApiService;

    /**
     * 授权用户权限
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        logger.debug("===============>开始授权用户权限...............");
        SysUserModel sysUserModel = (SysUserModel)principalCollection.getPrimaryPrincipal();
        Integer userId = sysUserModel.getId();
        // TODO 需要进行缓存处理，优化阶段进行处理，暂时先查数据库



        logger.debug("===============>结束授权用户权限...............");
        return null;
    }


    /**
     * 验证用户身份
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        logger.debug("===============>开始验证用户身份...............");
        logger.debug("===============>结束验证用户身份...............");
        return null;
    }

}
