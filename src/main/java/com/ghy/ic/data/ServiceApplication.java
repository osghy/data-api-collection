package com.ghy.ic.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * create by huweiqiang on 2018/5/18
 */
@SpringBootApplication(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class,
        DataSourceAutoConfiguration.class,
        SpringDataWebAutoConfiguration.class}
)
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
public class ServiceApplication  {
        //http://192.168.125.1:9025/sys/menu/list
    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class,args);
    }
}
